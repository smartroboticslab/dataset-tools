#!/bin/bash
# SPDX-FileCopyrightText: 2019-2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2019-2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

set -euo pipefail

dataset_basedir="$HOME/Documents/Datasets"
converter="./emanuelev-scene2raw-improved/scene2raw_tum"

# The timing association files produced by scene2raw-improved contain extra
# columns that do not play along well with TUM scripts. Create another version
# of the file the way TUM scripts expect it in that case.
fix_timing_association_files=1

# Loop over all datasets
for dataset_dir in "$dataset_basedir"/*/ ; do
	# Remove trailing /
	dataset_dir=${dataset_dir%*/}

	# Get the dataset type
	dataset_dir_name=$(basename "$dataset_dir")
	case "$dataset_dir_name" in
		*freiburg1*)
			tum_type=1
			;;
		*freiburg2*)
			tum_type=2
			;;
		*freiburg3*)
			tum_type=3
			;;
		*)
			tum_type=1
			;;
	esac

	# Construct the converter command
	converter_cmd="$converter --folder $dataset_dir --raw-output $dataset_dir/scene.raw --timing-assoc-output $dataset_dir/timings.assoc.txt --tum-type $tum_type"

	echo "Processing $dataset_dir as TUM $tum_type"
	eval "$converter_cmd"

	# Fix timing association files if needed.
	if [ "$fix_timing_association_files" -ne 0 ] ; then
		# Keep extended association file under another filename.
		cp "$dataset_dir/timings.assoc.txt" "$dataset_dir/timings.assoc_extended.txt"
		# Delete comment lines.
		sed -i '/^#/d' "$dataset_dir/timings.assoc.txt"
		# Keep only first column.
		cut -d ' ' -f 1 "$dataset_dir/timings.assoc.txt" > /tmp/timings.assoc.txt
		mv /tmp/timings.assoc.txt "$dataset_dir/timings.assoc.txt"
	fi
done

