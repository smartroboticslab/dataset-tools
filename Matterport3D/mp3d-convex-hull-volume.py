#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2022 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2022 Sotiris Papatheodorou, Imperial College London
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import multiprocessing
import os
import os.path
import trimesh

from typing import List, Tuple


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Approximate the volume of
            all Matterport3D meshes in MP3D_DIR by computing the volume of their
            convex hull and output the results in TSV format on stdandard
            output.""")
    parser.add_argument("dir", metavar="MP3D_DIR",
            help="The directory containing the Matterport3D sequences.")
    return parser.parse_args()


def mesh_filenames(dir: str) -> List[str]:
    filenames = []
    for root, _, files in os.walk(dir):
        for file in files:
            if file.endswith("_semantic.ply"):
                filenames.append(root + "/" + file)
    return filenames


def mesh_volume(filename: str) -> Tuple[str, float]:
    sequence = os.path.basename(os.path.dirname(filename))
    mesh = trimesh.load(filename, file_type='ply', force='mesh')
    hull = trimesh.convex.convex_hull(mesh)
    return sequence, hull.volume


if __name__ == "__main__":
    try:
        args = parse_args()
        print("Sequence\tConvex hull volume (m³)")
        with multiprocessing.Pool(2) as p:
                data = p.map(mesh_volume, mesh_filenames(args.dir), 1)
        data.sort(key=lambda x: x[0])
        for sequence, volume in data:
            print("{}\t{}".format(sequence, volume))
    except KeyboardInterrupt:
        pass
