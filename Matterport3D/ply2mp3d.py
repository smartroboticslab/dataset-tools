#!/usr/bin/env python3
import argparse
import numpy as np
import os.path
import plyfile


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
            description="""Convert PLY files to the PLY format required for
            Habitat-Sim Matterport3D datasets. Each converted file is saved with
            a _semantic.ply suffix.""")
    parser.add_argument("files", metavar="FILE", nargs="+")
    return parser.parse_args()


def convert_mesh(mesh):
    v_x = mesh["vertex"]["x"]
    v_y = mesh["vertex"]["y"]
    v_z = mesh["vertex"]["z"]
    try:
        v_r = mesh["vertex"]["red"]
        v_g = mesh["vertex"]["green"]
        v_b = mesh["vertex"]["blue"]
    except ValueError:
        v_r = np.full(v_x.shape, 255, dtype="u1")
        v_g = v_r
        v_b = v_r
    vertices = np.array(
            [(x, y, z, r, g, b) for x, y, z, r, g, b in zip(v_x, v_y, v_z, v_r,
                v_g, v_b)],
            dtype=[("x", "f4"), ("y", "f4"), ("z", "f4"),
                ("red", "u1"), ("green", "u1"), ("blue", "u1")])
    v = plyfile.PlyElement.describe(vertices, "vertex")

    vertex_index_name = [x.name for x in mesh["face"].properties if
            isinstance(x, plyfile.PlyListProperty)]
    f_i = mesh["face"][vertex_index_name]
    faces = np.array([(*f, 0) for f in f_i],
            dtype=[("vertex_indices", "i4", (3,)), ("object_id", "i4")])
    f = plyfile.PlyElement.describe(faces, "face")

    return plyfile.PlyData(elements=[v, f], comments=mesh.comments)


if __name__ == "__main__":
    try:
        args = parse_args()
        for file in args.files:
            print("Converting", file)
            with open(file, "rb") as f:
                mesh = plyfile.PlyData.read(f)
            new_mesh = convert_mesh(mesh)
            file_out = os.path.splitext(file)[0] + "_semantic.ply"
            new_mesh.write(file_out)
    except KeyboardInterrupt:
        pass
