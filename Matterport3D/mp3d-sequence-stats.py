#!/usr/bin/env python3
import argparse
import os
import os.path
import sys
import trimesh

class MeshStats:
    fields = {
            'dirname': 'Dirname',
            'x_min': 'Min x (m)',
            'y_min': 'Min y (m)',
            'z_min': 'Min z (m)',
            'x_max': 'Max x (m)',
            'y_max': 'Max y (m)',
            'z_max': 'Max z (m)',
            'x_range': 'Range x (m)',
            'y_range': 'Range y (m)',
            'z_range': 'Range z (m)',
            'x_center': 'Center x (m)',
            'y_center': 'Center y (m)',
            'z_center': 'Center z (m)',
            }

    def __init__(self, filename) -> None:
        mesh = trimesh.load(filename, file_type='ply', force='mesh')
        self.dirname = os.path.dirname(filename)
        self.x_min = float(mesh.bounds[0,0])
        self.y_min = float(mesh.bounds[0,1])
        self.z_min = float(mesh.bounds[0,2])
        self.x_max = float(mesh.bounds[1,0])
        self.y_max = float(mesh.bounds[1,1])
        self.z_max = float(mesh.bounds[1,2])
        self.x_range = self.x_max - self.x_min
        self.y_range = self.y_max - self.y_min
        self.z_range = self.z_max - self.z_min
        self.x_center = (self.x_min + self.x_max) / 2.0
        self.y_center = (self.y_min + self.y_max) / 2.0
        self.z_center = (self.z_min + self.z_max) / 2.0

    def __str__(self) -> str:
        data = [getattr(self, x) for x in MeshStats.fields]
        return "\t".join([str(x) for x in data])

    @staticmethod
    def tsvHeader() -> str:
        return "\t".join(MeshStats.fields.values())

def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name."""
    error_prefix = os.path.basename(sys.argv[0]) + " error:"
    print(error_prefix, *args, file=sys.stderr, **kwargs)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Compute statistics for
            one or more Matterport3D semantic PLY meshes and print them in TSV
            format on standard output.""")
    parser.add_argument("files", metavar="FILE", nargs="+",
            help="The PLY file to process.")
    return parser.parse_args()


if __name__ == "__main__":
    try:
        args = parse_args()
        print(MeshStats.tsvHeader())
        for file in args.files:
            try:
                print(MeshStats(file))
            except ValueError:
                printerr("not a PLY file:", file)
                continue
    except KeyboardInterrupt:
        pass
