#!/bin/sh
# SPDX-FileCopyrightText: 2021 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2021 Sotiris Papatheodorou, Imperial College London
# SPDX-License-Identifier: BSD-3-Clause
set -eu

usage() {
	printf 'Usage: %s [OPTIONS] CLASS_ID... < HOUSE_FILE\n\n' "${0##*/}"
	printf 'Given a Matterport3D .house file on standard input and\n'
	printf 'one or more class IDs, print the instance IDs of all objects\n'
	printf 'with matching class IDs in TSV format on standard output.\n\n'
	printf 'Valid class IDs are in the mpcat40index column here:\n'
	printf 'https://github.com/niessner/Matterport/blob/master/metadata/mpcat40.tsv\n'
	printf '\n  -h Display this help message\n'
}

# Usage: generate_class_array CLASS_ID...
generate_class_array() {
	while [ "$#" -gt 0 ]; do
		# We're only interested in the array keys, not the values, so set them
		# all to zero.
		printf 'mp40_class_ids[%d] = 0;' "$1"
		shift
	done
}



# Parse the command line options.
while getopts 'hv' opt_name ; do
	case "$opt_name" in
		h) # Display this help message
			usage
			exit 0
			;;
		*)
			usage
			exit 1
			;;
	esac
done
# Make $1 the first non-option argument.
shift "$((OPTIND - 1))"

# Ensure at least one class ID was provided.
if [ "$#" -eq 0 ]; then
	usage
	exit 2
fi

awk '
BEGIN {
	'"$(generate_class_array "$@")"'
	OFS = "\t"
	print "Instance ID", "Class ID", "Class name"
}
# Create an array from class IDs to strings containing the mp40 class ID and
# the respective name.
$1 == "C" {
	for (c in mp40_class_ids) {
		if ($5 == c) {
			classes[$2] = $5 "\t" $6
		}
	}
}
# Filter the object records keeping only class IDs contained in the array.
$1 == "O" {
	for (i in classes) {
		if ($4 == i) {
			print $2, classes[i]
		}
	}
}'

