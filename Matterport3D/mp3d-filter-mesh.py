#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2021 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2021 Nils Funk, Imperial College London
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import numpy
import os
import os.path
import plyfile
import sys

from typing import List

class Object:
    def __init__(self, instance_id, class_id, class_name):
        self.instance_id = int(instance_id)
        self.class_id = int(class_id)
        self.class_name = str(class_name)

    def filtered_mesh_name(self, filename: str) -> str:
        new_suffix = "_{}_{:04}.ply".format(self.class_name, self.instance_id)
        return os.path.basename(args.file).replace("_semantic.ply", new_suffix)



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Given a Matterport3D PLY
    mesh and objects in TSV format in standard input, generate one mesh for each
    object and save it in PLY format in the current directory.""")
    parser.add_argument("file", metavar="PLY_FILE",
            help="The Matterport3D PLY file to filter.")
    parser.add_argument("dir", metavar="DIRECTORY", nargs="?", default=".",
            help="The directory inside which the object meshes will be written.")
    return parser.parse_args()



def get_object_info() -> List[Object]:
    objects = []
    for line in sys.stdin:
        # Skip lines that don't start with a number.
        if not line[0].isnumeric():
            continue
        # Get the TSV columns.
        cols = line.rstrip("\r\n").split("\t")
        if len(cols) != 3:
            printerr("expected 3 TSV columns but got " + str(len(cols)))
            continue
        objects.append(Object(*cols))
    return objects



def filter_mesh(mesh: plyfile.PlyData, object: Object) -> plyfile.PlyData:
    # Create a boolean indexing list for the matching faces.
    matching_faces = [False] * len(mesh["face"])
    for i, face in enumerate(mesh["face"]):
        if face["object_id"] == object.instance_id:
            matching_faces[i] = True
    # Keep the faces with a matching instance ID.
    filtered_faces = mesh["face"][matching_faces]

    # Create a numpy array of the unique vertex indices of all vertices in
    # matching faces.
    matching_vertices = []
    for face in filtered_faces:
        matching_vertices = numpy.concatenate((matching_vertices,
            face["vertex_indices"]))
    matching_vertices.sort()
    matching_vertices = numpy.unique(matching_vertices).tolist()
    # Keep the vertices corresponding to matching faces.
    filtered_vertices = numpy.take(mesh["vertex"], matching_vertices)
    # Rewrite the vertex IDs in the matched faces.
    def idx_mapping(arr_in):
        arr_out = numpy.zeros(len(arr_in))
        for i in range(len(arr_in)):
            arr_out[i] = numpy.where(matching_vertices == arr_in[i])[0][0]
        return arr_out
    for face in filtered_faces:
        face["vertex_indices"] = idx_mapping(face["vertex_indices"])
    # Create a new mesh with the filtered vertices and faces.
    vertices = plyfile.PlyElement.describe(filtered_vertices, "vertex")
    faces = plyfile.PlyElement.describe(filtered_faces, "face")
    return plyfile.PlyData([vertices, faces], text=False)



if __name__ == "__main__":
    try:
        args = parse_args()
        args.dir += "/"
        objects = get_object_info()
        print("Loading " + args.file)
        with open(args.file, "rb") as f:
            mesh = plyfile.PlyData.read(f)
        for object in objects:
            print("Filtering {} {}".format(object.class_name, object.instance_id))
            os.makedirs(args.dir, exist_ok=True)
            object_filename = args.dir + object.filtered_mesh_name(args.file)
            filter_mesh(mesh, object).write(object_filename)
    except KeyboardInterrupt:
        pass

