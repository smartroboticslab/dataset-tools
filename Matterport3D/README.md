# Matterport3D tools

## mp3d-objects-of-class.sh

Given a Matterport3D .house file on standard input and one or more class IDs,
print the instance IDs of all objects with matching class IDs in TSV format on
standard output.

Example usage:

``` sh
./mp3d-objects-of-class.sh 8 10 < ~/Matterport3D/2t7WUuJeko7/2t7WUuJeko7.house
```

Output (with columns aligned using spaces):

``` text
Instance ID   Class ID   Class name
28            10         sofa
161           8          cushion
```

Valid class IDs are in the mpcat40index column
[here](https://github.com/niessner/Matterport/blob/master/metadata/mpcat40.tsv).

## mp3d-filter-mesh.py

Given a Matterport3D PLY mesh and objects in TSV format in standard input,
generate one mesh for each object and save it in PLY format in the current
directory.

This script is designed to receive the output of `mp3d-objects-of-class.sh`.
Example usage:

``` sh
./mp3d-objects-of-class.sh 8 10 < ~/Matterport3D/2t7WUuJeko7/2t7WUuJeko7.house \
    | ./mp3d-filter-mesh.py ~/Matterport3D/2t7WUuJeko7/2t7WUuJeko7_semantic.ply
```

Resulting files:

``` text
2t7WUuJeko7_semantic_cushion_161.ply
2t7WUuJeko7_semantic_sofa_28.ply
```

