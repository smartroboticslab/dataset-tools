#!/bin/sh
set -eu

if [ "$#" -lt 3 ]
then
	printf 'Usage: %s MP3D_DIR OBJECT_DIR CLASS_ID ...\n' "${0##*/}"
	exit 2
fi

script_dir="${0%/*}"
input_dir="${1%%/}"
shift
output_dir="${1%%/}"
shift

find "$input_dir" -type f -name '*.house' | while IFS= read -r house
do
	name="${house%.house}"
	name="${name##*/}"
	mesh="${house%.house}_semantic.ply"
	"$script_dir/mp3d-objects-of-class.sh" "$@" < "$house" |
		"$script_dir/mp3d-filter-mesh.py" "$mesh" "$output_dir/matterport3d_$name"
done
