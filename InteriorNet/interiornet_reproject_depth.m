% SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
% SPDX-FileCopyrightText: 2020 Marija Popović
% SPDX-FileCopyrightText: 2021 Sotiris Papatheodorou
% SPDX-License-Identifier: BSD-3-Clause

clear all;
close all;

% Change to the directory containing the input PNG images
depth_directory = 'depth0/data';

% Find the PNG images to reproject
depth_files = dir(fullfile(depth_directory, '*.png'));
if isempty(depth_files)
	printf('Error: directory %s contains no PNG images\n', depth_directory);
	return
end

% Create the output directory
out_directory = strcat(depth_directory, '_reprojected');
if ~mkdir(out_directory)
	printf('Error: could not create output directory %s\n', out_directory);
	return
end

% Generate the reprojection matrix
w = 640;
h = 480;
reprojection = zeros(h, w);
for y = 1:h
	for x = 1:w
		w2 = 15/16 * w;
		reprojection(y,x) = sqrt((1 + (x - w/2) * (x - w/2) / (w2 ^ 2) + (y - h/2) * (y - h/2) / (w2 ^ 2)));
	end
end

% Scale each depth image
for i=1:length(depth_files)
	filename_in = fullfile(depth_directory, depth_files(i).name);
	depth_img = imread(filename_in);

	% Scale the depth values to metres, reproject and re-scale to millimetres
	depth = double(depth_img) / 1000;
	depth = depth ./ reprojection;
	depth = depth * 1000;

	filename_out = fullfile(out_directory, depth_files(i).name);
	imwrite(uint16(depth), filename_out);
	fprintf('\r%4d/%4d', i, length(depth_files));
end

fprintf('\nImages written to %s\n', out_directory);

