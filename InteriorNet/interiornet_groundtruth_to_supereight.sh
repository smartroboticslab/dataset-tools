#!/bin/sh
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2021 Sotiris Papatheodorou, Imperial College London
# SPDX-License-Identifier: BSD-3-Clause
set -eu

usage() {
	printf "Usage: %s FILE...\n" "$(basename "$0")"
	printf "  Convert each InteriorNet ground truth FILE into a
  supereight-compatible ground truth FILE.se.txt.\n"
}

if [ "$#" -eq 0 ]; then
	usage
	exit 2
fi

while [ "$#" -eq 0 ]; do
	# Change commas to spaces (OFS is a space by default) and reorder the
	# quaternion from w x y z to x y z w.
	awk 'BEGIN { FS=","}; { print $1, $2, $3, $4, $6, $7, $8, $5 }' "$1" > "$1".se.txt
	shift
done

