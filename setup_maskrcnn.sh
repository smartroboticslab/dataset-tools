#!/bin/bash
# SPDX-FileCopyrightText: 2019-2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2019-2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# Set up a Python virtualenv with the Mask R-CNN implementation from:
#   https://github.com/facebookresearch/maskrcnn-benchmark

# Select operations to perform. 1 enables, 0 disables.
# Install CUDA.
setup_cuda=0
# Install Miniconda3
setup_miniconda=0
# Create a Miniconda environment with the required dependencies.
setup_maskrcnn_env=1
# Clone the repository and setup.
setup_maskrcnn=1



# Set Bash settings.
set -euo pipefail
IFS=$'\n\t'

# Setup CUDA ###################################################################
cuda_url='https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.1.105-1_amd64.deb'
cuda_key='https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub'
cuda_deb="/tmp/$(basename "$cuda_url")"

if [[ "$setup_cuda" -eq 1 ]] ; then
	echo
	echo "Installing CUDA #####################################################"
	echo 'Update the cuda_url and cuda_key variables with the latest CUDA (deb network) version from here:'
	echo 'https://developer.nvidia.com/cuda-downloads'
	echo 'Also see installation guide from here:'
	echo 'https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html'
	echo 'Press ENTER to continue'
	read

	wget -O "$cuda_deb" "$cuda_url"
	sudo dpkg -i "$cuda_deb"
	sudo apt-key adv --fetch-keys "$cuda_key"
	sudo apt update
	sudo apt install cuda

	echo 'Add the following command to your .bashrc:'
	echo 'export PATH=/usr/local/cuda-10.1/bin:/usr/local/cuda-10.1/NsightCompute-2019.1${PATH:+:${PATH}}'
	echo 'Reboot the computer before continuing with the setup'
	echo 'Press ENTER to quit setup'
	read
	exit 0
else
	echo
	echo "Skipping CUDA installation ##########################################"
fi



# Setup Miniconda ##############################################################
miniconda_url='https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh'
miniconda_instaler="/tmp/$(basename "$miniconda_url")"
miniconda_dir="$HOME/Documents/miniconda3"

if [[ "$setup_miniconda" -eq 1 ]] ; then
	echo
	echo "Installing and updating Miniconda ###################################"

	# Download and install.
	wget -O "$miniconda_instaler" "$miniconda_url"
	chmod +x "$miniconda_instaler"
	"$miniconda_instaler" -p "$miniconda_dir"
	rm -f "$miniconda_instaler"

	# Update conda.
	source "$miniconda_dir/bin/activate"
	conda update -y -n base -c defaults conda
	conda deactivate
else
	echo
	echo "Skipping Miniconda installation #####################################"
fi



# Setup Mask-RCNN Miniconda environment ########################################
maskrcnn_env='maskrcnn'

if [[ "$setup_maskrcnn_env" -eq 1 ]] ; then
	echo
	echo "Seting up maskrcnn environment ######################################"

	source "$miniconda_dir/bin/activate"
	conda create --name "$maskrcnn_env"

	conda activate "$maskrcnn_env"
	conda install -y ipython
	conda install -y -c menpo opencv
	pip install --upgrade pip
	pip install opencv-contrib-python
	pip install ninja yacs cython matplotlib tqdm
	conda install -y -c pytorch pytorch-nightly torchvision cudatoolkit
	conda deactivate

	conda deactivate
else
	echo
	echo "Skipping maskrcnn environment setup #################################"
fi



# Setup Mask-RCNN ##############################################################
repo_dir="$miniconda_dir/envs/$maskrcnn_env/source"

if [[ "$setup_maskrcnn" -eq 1 ]] ; then
	echo
	echo "Seting up Mask-RCNN #################################################"

	# Clone required repositories.
	mkdir -p "$repo_dir"
	git clone --depth 1 https://github.com/cocodataset/cocoapi.git "$repo_dir/cocoapi"
	git clone --depth 1 https://github.com/facebookresearch/maskrcnn-benchmark.git "$repo_dir/maskrcnn-benchmark"

	source "$miniconda_dir/bin/activate"

	conda activate "$maskrcnn_env"
	# Run in subshell.
	( cd "$repo_dir/cocoapi/PythonAPI"; python setup.py build_ext install )
	( cd "$repo_dir/maskrcnn-benchmark"; python setup.py build develop )
	conda deactivate

	conda deactivate

	# Create hard link to the script. Symbolic links will not work correctly.
	ln "./maskrcnn_on_dataset.py" "$repo_dir/maskrcnn-benchmark/demo"
else
	echo
	echo "Skipping Mask-RCNN setup ############################################"
fi

