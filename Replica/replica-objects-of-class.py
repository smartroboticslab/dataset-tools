#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2022 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2022 Sotiris Papatheodorou, Imperial College London
# SPDX-License-Identifier: BSD-3-Clause
import argparse
import json
import sys

from typing import Dict, List

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Given a Replica dataset
    habitat/info_semantic.json on standard input and zero or more class IDs,
    print the instance IDs of all objects' with matching class IDs in TSV format
    on standard output. With no class IDs print all objects in the dataset.""")
    parser.add_argument("class_ids", metavar="CLASS_ID", nargs="*", type=int,
            help="An integer class ID.")
    return parser.parse_args()

def filter_classes(json_object, class_ids: List[int]) -> None:
    print("Instance ID\tClass ID\tClass name")
    for o in json_object["objects"]:
        if not class_ids or o["class_id"] in class_ids:
            print("{}\t{}\t{}".format(o["id"], o["class_id"], o["class_name"]))

if __name__ == "__main__":
    try:
        args = parse_args()
        filter_classes(json.load(sys.stdin), args.class_ids)
    except KeyboardInterrupt:
        pass

