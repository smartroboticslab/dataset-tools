# Dataset Tools

Scripts for downloading, converting and manipulating datasets. The folders
contain tools for specific datasets. There are also scripts to run whole
datasets through neural networks and export the results.



## License

Copyright © 2019-2020 Smart Robotics Lab, Imperial College London

Most files are distributed under the BSD 3-Clause license. Check the individual
files for their license.

