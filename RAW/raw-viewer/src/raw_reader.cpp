// SPDX-FileCopyrightText: 2022 Smart Robotics Lab, Imperial College London, Technical University of Munich
// SPDX-FileCopyrightText: 2022 Sotiris Papatheodorou
// SPDX-License-Identifier: BSD-3-Clause

#include <type_traits>

#include "raw_reader.hpp"

struct RGB {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

bool read_resolution(std::ifstream& f, uint32_t& width, uint32_t& height)
{
    return f.read(reinterpret_cast<char*>(&width), sizeof(width))
           and f.read(reinterpret_cast<char*>(&height), sizeof(height));
}



template<typename T>
bool read_next_image(std::ifstream& f, cv::Mat& image)
{
    static_assert(std::is_same<T, RGB>::value or std::is_same<T, uint16_t>::value,
                  "only defined for RGB and 16-bit depth images");
    uint32_t w = 0;
    uint32_t h = 0;
    if (!read_resolution(f, w, h)) {
        return false;
    }
    if (w == 0 or h == 0) {
        throw std::runtime_error("Invalid image resolution " + std::to_string(w) + "x" + std::to_string(h));
    }
    const int type = std::is_same<T, uint16_t>::value ? CV_16UC1 : CV_8UC3;
    image.create(cv::Size(w, h), type);
    // Read the image data row-by-row.
    for (int r = 0; r < image.rows; ++r) {
        char* row_data = image.ptr<char>(r);
        if (!f.read(row_data, image.cols * sizeof(T))) {
            return false;
        }
    }
    return true;
}



RawReader::RawReader(const std::string& filename)
    : filename_(filename)
{
    f_.open(filename_, std::ios::in | std::ios::binary);
    if (!f_.good()) {
        throw std::invalid_argument("Error reading RAW file: " + filename);
    }
    cv::Mat depth_image, colour_image;
    while (true) {
        if (readNextDepth(depth_image)) {
            const std::string res = std::to_string(depth_image.cols) + "x" + std::to_string(depth_image.rows);
            num_depth_images_[res]++;
        } else {
            break;
        }
        if (readNextColour(colour_image)) {
            const std::string res = std::to_string(depth_image.cols) + "x" + std::to_string(depth_image.rows);
            num_colour_images_[res]++;
        } else {
            break;
        }
    }
    f_.clear();
    f_.seekg(0, std::ios_base::beg);
}



bool RawReader::readNextImages(cv::Mat& depth_image, cv::Mat& colour_image)
{
    return readNextDepth(depth_image) and readNextColour(colour_image);
}



bool RawReader::readNextDepth(cv::Mat& depth_image)
{
    return read_next_image<uint16_t>(f_, depth_image);
}



bool RawReader::readNextColour(cv::Mat& colour_image)
{
    return read_next_image<RGB>(f_, colour_image);
}



std::ostream& operator<<(std::ostream& os, const RawReader& r)
{
    os << "Depth\n";
    for (auto p : r.num_depth_images_) {
        os << p.first << ": " << p.second << " images\n";
    }
    os << "Colour\n";
    for (auto p : r.num_colour_images_) {
        os << p.first << ": " << p.second << " images\n";
    }
    return os;
}
