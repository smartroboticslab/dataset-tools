// SPDX-FileCopyrightText: 2022 Smart Robotics Lab, Imperial College London, Technical University of Munich
// SPDX-FileCopyrightText: 2022 Sotiris Papatheodorou
// SPDX-License-Identifier: BSD-3-Clause

#include <string>

#include "raw_reader.hpp"

void usage()
{
    std::cout << "Usage: raw-viewer FILE\n";
}



int main(int argc, char** argv)
{
    if (argc != 2) {
        usage();
        return EXIT_FAILURE;
    }
    const std::string filename (argv[1]);
    RawReader reader(filename);
    std::cout << reader;

    cv::Mat depth, colour, depth_render;
    bool quit = false;
    while (not quit) {
        reader.readNextImages(depth, colour);
        cv::Mat depth_8(depth.rows, depth.cols, CV_8UC1);
        depth.convertTo(depth_8, CV_8UC1, 1.0 / UINT8_MAX);
        cv::applyColorMap(depth_8, depth_render, cv::COLORMAP_JET);
        cv::Mat render(depth_render.rows + colour.rows, depth_render.cols, CV_8UC3);
        cv::vconcat(depth_render, colour, render);
        cv::imshow("raw-viewer", render);
        switch (cv::waitKey(0)) {
            case 'q':
                quit = true;
        }
    }

    return EXIT_SUCCESS;
}
