// SPDX-FileCopyrightText: 2022 Smart Robotics Lab, Imperial College London, Technical University of Munich
// SPDX-FileCopyrightText: 2022 Sotiris Papatheodorou
// SPDX-License-Identifier: BSD-3-Clause

#ifndef RAW_READER_HPP
#define RAW_READER_HPP

#include <cstdint>
#include <fstream>
#include <map>

#include <opencv2/opencv.hpp>

class RawReader {
    public:
    RawReader(const std::string& filename);

    bool readNextImages(cv::Mat& depth_image, cv::Mat& colour_image);

    friend std::ostream& operator<<(std::ostream& os, const RawReader& r);

    private:
    const std::string filename_;
    std::ifstream f_;
    std::map<std::string, size_t> num_depth_images_;
    std::map<std::string, size_t> num_colour_images_;

    bool readNextDepth(cv::Mat& depth_image);
    bool readNextColour(cv::Mat& colour_image);
};


#endif // RAW_READER_HPP
