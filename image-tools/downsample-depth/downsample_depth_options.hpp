/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __DOWNSAMPLE_DEPTH_OPTIONS_HPP
#define __DOWNSAMPLE_DEPTH_OPTIONS_HPP

#include <argp.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "downsample_depth_utils.hpp"



/** Struct containing the program options.
 */
struct Options {
    std::vector<std::string> input_files;
    int downsampling_factor = 1;
    int verbose = 0;
    bool prompt = true;
};



/** Stream operator for Options struct. It is intended for printing to
 * std:cout.
 */
static std::ostream& operator<<(std::ostream& out, const Options& opts) {
    out << "Downsampling factor: " << opts.downsampling_factor << "\n"
        << "Verbosity level:     " << opts.verbose << "\n";
    out << "Input files:\n";
    for (const auto& file : opts.input_files) {
        out << "  " << file << "\n";
    }
    return out;
}



/** Parser for argp.
 */
static error_t argp_parser(int key, char* arg, struct argp_state* state) {
    Options* opts = static_cast<Options*>(state->input);

    switch (key) {
        case 'd':
            opts->downsampling_factor = atoi(arg);
            if (opts->downsampling_factor <= 0) {
                std::cerr << colour_error("Error:")
                    + " Downsampling-factor must be a positive integer, not " << arg << "\n";
                exit(2);
            }
            break;

        case 'y':
            opts->prompt = false;
            break;

        case 'v':
            (opts->verbose)++;
            break;

        case ARGP_KEY_ARG:
            opts->input_files.push_back(arg);
            break;

        case ARGP_KEY_END:
            if (state->arg_num < 1) {
                std::cerr << colour_error("Error:") + " Too few arguments\n";
                argp_usage(state);
            }
            // Ensure that only PNG images were supplied.
            for (const auto& filename : opts->input_files) {
                if (filename.size() < 4 || filename.substr(filename.size() - 4, 4) != ".png") {
                    std::cerr << colour_error("Error:")
                            + " Only PNG images are supported. Got " << filename << "\n";
                    exit(2);
                }
            }
            // Print the command line options.
            if (opts->verbose >= 1) {
                std::cout << *opts;
            }
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}



/** Parse the command line arguments and return a struct containing them.
 */
Options parse_args(int argc, char** argv) {
    // Program help and command line argument declaration.
    const char doc[] = "\nDownsample image FILEs in-place.";
    const char args_doc[] = "FILE [...]";
    const struct argp_option options[] = {
        {"downsampling-factor", 'd', "N", 0,
                "The factor by which the image will be downsampled. "
                "Must be a positive integer.\n"
                "(Default: 1)", 0},
        {"yes",                 'y', 0,   0,
                "Automatic \"yes\" to all prompts.", 0},
        {"verbose",             'v', 0,   0,
                "Produce more verbose output. Extra occurrences of this "
                "option, up to 1 total, increase the amount of information "
                "shown.", 0},
        {0, 0, 0, 0, 0, 0}
    };

    Options opts;
    const struct argp argp_settings = {options, argp_parser, args_doc, doc, 0, 0, 0};
    argp_parse(&argp_settings, argc, argv, 0, 0, &opts);
    return opts;
}

#endif // __DOWNSAMPLE_DEPTH_OPTIONS_HPP

