/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __DOWNSAMPLE_DEPTH_UTILS_HPP
#define __DOWNSAMPLE_DEPTH_UTILS_HPP

#include <algorithm>
#include <string>
#include <vector>



/** Compute the median of the data in the vector.
 * If the vector has an even number of elements, the second of the two
 * middle elements will be returned instead of their average. This is done
 * to avoid creating values that don't exist in the original data.
 *
 * \param[in,out] data The data to compute the median of. The vector will
 *                     be sorted in-place.
 * \return The median of the data. If input vector is empty, the value
 * returned by the constructor T() is returned. This is typically 0.
 *
 * \warning The vector will be sorted inside this function.
 *
 * \note Weird things will happen if the vector contains NaNs.
 */
template <typename T>
static T almost_median(std::vector<T>& data) {
  if (!data.empty()) {
    std::sort(data.begin(), data.end());
    const size_t mid_idx = data.size() / 2;
    return data[mid_idx];
  } else {
    return T();
  }
}



/** Add ANSI escape sequences to the string so that it's shown coloured in a
 * terminal.
 */
static std::string colour_warn(const std::string& s) {
    return "\x1B[1;93m" + s + "\x1B[m";
}



/** Add ANSI escape sequences to the string so that it's shown coloured in a
 * terminal.
 */
static std::string colour_error(const std::string& s) {
    return "\x1B[1;91m" + s + "\x1B[m";
}

#endif // __DOWNSAMPLE_DEPTH_UTILS_HPP

