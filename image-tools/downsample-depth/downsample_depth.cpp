/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "downsample_depth.hpp"

int main(int argc, char** argv) {
    // Read the options
    const Options opts = parse_args(argc, argv);
    // Prompt the user for confirmation
    if (opts.prompt) {
        std::cout << colour_warn("Warning:") << " You're about to overwrite "
                << colour_warn(std::to_string(opts.input_files.size())) << " images.\n";
        while (true) {
            std::cout << "Confirm the number of images to proceed: ";
            std::string line;
            std::cin >> line;
            size_t input = 0;
            try {
                input = std::stoull(line);
            } catch (const std::invalid_argument&) {
            } catch (const std::out_of_range&) {
            }
            if (input == opts.input_files.size()) {
                break;
            }
        }
    }
    // Iterate over each input image
#pragma omp parallel for
    for (const auto& filename : opts.input_files) {
        // Read the input image
        std::vector<uint8_t> data;
        uint32_t width;
        uint32_t height;
        if (load_tum_depth(filename, data, width, height)) {
            std::cerr << colour_error("Error:") << " Could not read file " << filename << "\n";
            exit(EXIT_FAILURE);
        }
        // Downsample the image
        if (downsample_tum_depth(data, width, height, opts.downsampling_factor) == 2) {
            std::cerr << colour_warn("Warning:") << " Too low resolution to downsample "
                    << filename << "\n";
            continue;
        }
        // Save the image
        if (save_tum_depth(filename, data, width, height)) {
            std::cerr << colour_error("Error:") << " Could not write file " << filename << "\n";
            exit(EXIT_FAILURE);
        }
#pragma omp critical (ProgressPrint)
        std::cout << "Downsampled " << filename << "\n";
    }
    exit(EXIT_SUCCESS);
}

