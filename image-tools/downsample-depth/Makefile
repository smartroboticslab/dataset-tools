# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: CC0-1.0

# downsample-depth Makefile

# Executable name.
EXECUTABLE = downsample-depth

# Source files to compile.
CXXHEADERS = lodepng.h downsample_depth_options.hpp downsample_depth_utils.hpp downsample_depth.hpp
CXXSOURCES = lodepng.cpp downsample_depth.cpp

# Preprocessor, compiler and linker flags.
CPPFLAGS +=
CFLAGS = -std=c++11 -Wall -Wextra -pedantic -Wno-unused-function -fopenmp
LDLIBS = -lm

PREFIX ?= /usr/local
_INSTDIR = $(DESTDIR)$(PREFIX)
BINDIR ?= $(_INSTDIR)/bin



.PHONY: all
all: release

bin/$(EXECUTABLE): $(CXXHEADERS) $(CXXSOURCES)
	mkdir -p $(@D)
	$(CXX) $(CFLAGS) $(CPPFLAGS) -o bin/$(EXECUTABLE) $(CXXSOURCES) $(LDLIBS)

.PHONY: release
release: CFLAGS += -DNDEBUG -O3
release: bin/$(EXECUTABLE)

.PHONY: debug
debug: CFLAGS += -g -Werror
debug: bin/$(EXECUTABLE)


.PHONY: install
install: release
	install -D bin/$(EXECUTABLE) $(BINDIR)/$(EXECUTABLE)

.PHONY: uninstall
uninstall:
	rm -f $(BINDIR)/$(EXECUTABLE)

.PHONY: clean
clean:
	rm -rf bin

