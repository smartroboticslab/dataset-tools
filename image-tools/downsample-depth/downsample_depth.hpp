/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __DOWNSAMPLE_DEPTH_HPP
#define __DOWNSAMPLE_DEPTH_HPP

#include "lodepng.h"

#include "downsample_depth_options.hpp"



/** Read a TUM depth PNG image.
 */
static int load_tum_depth(const std::string&    filename,
                          std::vector<uint8_t>& data,
                          uint32_t&             width,
                          uint32_t&             height) {
    data.clear();
    width = 0;
    height = 0;
    // Load the raw PNG data.
    std::vector<uint8_t> png_data;
    lodepng::load_file(png_data, filename);
    // Decode the PNG data. The output format must be gray (16 bits per pixel).
    lodepng::State state;
    state.decoder.color_convert = true;
    state.info_raw.colortype = LCT_GREY;
    state.info_raw.bitdepth = 16;
    const unsigned err = lodepng::decode(data, width, height, state, png_data);
    if (err) {
        std::cerr << "LodePNG error for file " << filename << "\n"
                << "  " << lodepng_error_text(err) << "\n";
        return 1;
    }
    // Swap the byte order from big endian to little endian.
    for (size_t i = 0; i < data.size() - 1; i += 2) {
        std::swap(data[i], data[i + 1]);
    }
    return 0;
}



/** Read a TUM RGB PNG image.
 */
static int load_tum_rgb(const std::string&    filename,
                        std::vector<uint8_t>& data,
                        uint32_t&             width,
                        uint32_t&             height) {
    data.clear();
    width = 0;
    height = 0;
    // Load the raw PNG data.
    std::vector<uint8_t> png_data;
    lodepng::load_file(png_data, filename);
    // Decode the PNG data. The output format must be RGB (24 bits per pixel).
    lodepng::State state;
    state.decoder.color_convert = true;
    state.info_raw.colortype = LCT_RGB;
    state.info_raw.bitdepth = 8;
    const unsigned err = lodepng::decode(data, width, height, state, png_data);
    if (err) {
        std::cerr << "LodePNG error for file " << filename << "\n"
                << "  " << lodepng_error_text(err) << "\n";
        return 1;
    }
    return 0;
}



/** Writea TUM depth PNG image.
 */
static int save_tum_depth(const std::string&          filename,
                          const std::vector<uint8_t>& data,
                          const uint32_t              width,
                          const uint32_t              height) {
    // Swap the byte order from little endian to big endian.
    std::vector<uint8_t> bigendian_data (data);
    for (size_t i = 0; i < bigendian_data.size() - 1; i += 2) {
        std::swap(bigendian_data[i], bigendian_data[i + 1]);
    }
    // Encode the PNG data. The output format must be gray (16 bits per pixel).
    const unsigned err = lodepng::encode(filename, bigendian_data, width, height, LCT_GREY, 16);
    if (err) {
        std::cerr << "LodePNG error for file " << filename << "\n"
                << "  " << lodepng_error_text(err) << "\n";
        return 1;
    }
    return 0;
}



/** Downsample the depth image using median downsampling.
 * Return 0 on success, 1 for invalid downsampling factor, 2 for too low input resolution.
 */
static int downsample_tum_depth(std::vector<uint8_t>& depth_data,
                                uint32_t&             depth_width,
                                uint32_t&             depth_height,
                                int                   downsampling_factor) {
    // Check for valid input arguments
    if (downsampling_factor == 1) {
        return 0;
    } else if (downsampling_factor <= 0) {
        return 1;
    }
    // Check for sufficient image resolution
    if (depth_width <= 1 || depth_height <= 1) {
        return 2;
    }
    // Initialize the output image
    const uint32_t output_width = depth_width / downsampling_factor;
    const uint32_t output_height = depth_height / downsampling_factor;
    std::vector<uint8_t> output_data (sizeof(uint16_t) * output_width * output_height);
    // Reinterpret the data as uint16_t
    const uint16_t* input_depth = reinterpret_cast<const uint16_t*>(depth_data.data());
    uint16_t* output_depth = reinterpret_cast<uint16_t*>(output_data.data());
    // Downsample the image
    for (unsigned y_out = 0; y_out < output_height; y_out++) {
        for (unsigned x_out = 0; x_out < output_width; x_out++) {
            std::vector<float> box_values;
            box_values.reserve(downsampling_factor * downsampling_factor);
            for (int b = 0; b < downsampling_factor; b++) {
                for (int a = 0; a < downsampling_factor; a++) {
                    const int y_in = y_out * downsampling_factor + b;
                    const int x_in = x_out * downsampling_factor + a;
                    const uint16_t depth_value = input_depth[x_in + depth_width * y_in];
                    // Only consider positive values for the median
                    if (depth_value > 0) {
                        box_values.push_back(depth_value);
                    }
                }
            }
            output_depth[x_out + output_width * y_out] = box_values.empty() ? 0 : almost_median(box_values);
            box_values.clear();
        }
    }
    // Copy the output data.
    depth_data = output_data;
    depth_width = output_width;
    depth_height = output_height;
    return 0;
}

#endif // __DOWNSAMPLE_DEPTH_HPP

