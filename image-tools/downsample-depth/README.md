# downsample-depth

Downsample depth images using median downsampling. Invalid depth values (`0`) are not taken into
account. The median will not average depth values to avoid creating unobserved measurements.



## Build

To compile the executable and place it inside the `bin/` directory just run `make`. Only a compiler
implementing the C++11 standard is required.



## Usage

**CAUTION:** This tool will downsample images in-place. Make sure you have a backup before running it.

To downsample all depth images contained in `/path/to/depth/` by a factor of 2 run

``` bash
./bin/downsample-depth -d 2 /path/to/depth/*.png
```



## Known Issues

- Only 16-bit grayscale PNG images are supported. Anything else will have unexpected results.
- PNG images are detected by their extension (`.png`), not by their contents.
- Images are downsampled in-place instead of in a different folder.



## License

Copyright © 2020 Smart Robotics Lab, Imperial College London

Distributed under the BSD 3-Clause license.

