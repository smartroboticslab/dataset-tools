#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2019-2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2019-2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# Run from maskrcnn-benchmark/demo
# Example command:
# python3 maskrcnn_on_dataset.py "$HOME/Documents/Datasets/rgbd_dataset_freiburg1_desk"

import os
import sys
import time

import cv2
import numpy as np
from maskrcnn_benchmark.config import cfg
from predictor import COCODemo



# Number of classes
num_classes = 80



# Load a config file.
print('Initializing neural network')
config_file = '../configs/caffe2/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml'
cfg.merge_from_file(config_file)

# Setup Mask-RCNN.
coco_demo = COCODemo(cfg, min_image_size=800, confidence_threshold=0.7)



# Get the full paths of all images in the supplied folder.
base_dir = sys.argv[1]
image_dir = base_dir + '/rgb'
image_paths = []
with os.scandir(image_dir) as it:
    for entry in it:
        if entry.name.endswith('.png') and entry.is_file():
            image_paths.append(image_dir + '/' + entry.name)
image_paths.sort()



# Create the folders to export the results to.
segmentation_dir = base_dir + '/segmentation_maskrcnn'
if not os.path.exists(segmentation_dir):
    os.makedirs(segmentation_dir)
# Create a dictionary with paths to the various export directories.
export_dir_names = ['boxes', 'class_ids', 'confidence', 'confidence_all', 'masks', 'visualization']
export_dirs = {}
for name in export_dir_names:
    dir_name = segmentation_dir + '/' + name
    export_dirs[name] = dir_name
    # Create the directory.
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)



# Iterate over all images.
image_num = 1
for image_path in image_paths:

    # Load the image.
    image = cv2.imread(image_path)

    # Run inference on the image and keep only the predictions above the
    # confidence_threshold.
    start_time = time.time()
    predictions = coco_demo.compute_prediction(image)
    predictions = coco_demo.select_top_predictions(predictions)
    end_time = time.time()
    elapsed_time = end_time - start_time

    # Get the results.
    num_detections = len(predictions)
    boxes = predictions.bbox.numpy()
    class_ids = predictions.get_field('labels').numpy()
    confidence = predictions.get_field('scores').numpy()
    # All masks are single channel images. Reshape to remove singleton
    # dimension.
    masks = predictions.get_field('mask').numpy() \
            .reshape(num_detections, image.shape[0], image.shape[1])
    # TODO get this from the network
    confidence_all = np.zeros((num_detections, num_classes), 'f4')
    for d in range(num_detections):
        confidence_all[d][class_ids[d]] = confidence[d]
    # Create a visualization
    visualization = image.copy()
    visualization = coco_demo.overlay_boxes(visualization, predictions)
    visualization = coco_demo.overlay_mask(visualization, predictions)
    visualization = coco_demo.overlay_class_names(visualization, predictions)

    # Print results.
    print('{:5d} / {:d}  {:3.2f}%  {}  {:2d}  {:.2f} s'\
            .format(image_num, len(image_paths),
            100.0 * image_num / len(image_paths),
            image_path, num_detections, elapsed_time))

    # Export the results.
    image_basename = os.path.splitext(os.path.basename(image_path))[0]
    npy_filename = image_basename + '.npy'
    png_filename = image_basename + '.png'
    np.save(export_dirs['boxes']          + '/' + npy_filename, boxes)
    np.save(export_dirs['class_ids']      + '/' + npy_filename, class_ids)
    np.save(export_dirs['confidence']     + '/' + npy_filename, confidence)
    np.save(export_dirs['confidence_all'] + '/' + npy_filename, confidence_all)
    np.save(export_dirs['masks']          + '/' + npy_filename, masks)
    cv2.imwrite(export_dirs['visualization'] + '/' + png_filename, visualization)

    image_num += 1

