#!/usr/bin/env python3

import argparse
import numpy
import os
import plyfile
import sys



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Change the storage foramt of a PLY mesh.""")
    parser.add_argument("format", metavar="FORMAT",
            choices=["ascii", "binary", "binary_be", "binary_le"],
            help="""The format of the saved PLY file. \"binary\" will use
            the system endianness.""")
    parser.add_argument("input_file", metavar="INPUT_PLY",
            help="The PLY file to convert.")
    parser.add_argument("output_file", metavar="OUTPUT_PLY",
            help="The converted PLY file.")
    return parser.parse_args()



def set_mesh_format(mesh, format: str) -> None:
    if format == "ascii":
        mesh.text = True
    elif format == "binary_le" or (format == "binary" and sys.byteorder == "little"):
        mesh.text = False
        mesh.byte_order = "<"
    else:
        mesh.text = False
        mesh.byte_order = ">"



if __name__ == "__main__":
    try:
        args = parse_args()
        with open(args.input_file, "rb") as f:
            mesh = plyfile.PlyData.read(f)
        set_mesh_format(mesh, args.format)
        mesh.write(args.output_file)
    except KeyboardInterrupt:
        pass

