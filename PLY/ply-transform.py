#!/usr/bin/env python3

import argparse
import numpy
import os
import plyfile
import sys



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="""Transform a PLY mesh.""")
    parser.add_argument("input_file", metavar="INPUT_PLY",
            help="The PLY file to transform.")
    parser.add_argument("output_file", metavar="OUTPUT_PLY",
            help="The transofmed PLY file.")
    parser.add_argument("-f", "--format", default="binary",
            choices=["ascii", "binary", "binary_be", "binary_le"],
            help="""The format of the saved PLY file. \"binary\" will use
            the system endianness.""")
    parser.add_argument("-t", "--translation", metavar="X,Y,Z", default="0,0,0",
            help="The translation to apply.")
    args = parser.parse_args()
    args.translation = [float(x) for x in args.translation.split(",")]
    args.T = numpy.identity(4)
    args.T[0,3] = args.translation[0]
    args.T[1,3] = args.translation[1]
    args.T[2,3] = args.translation[2]

    return args



def set_mesh_format(mesh, format: str) -> None:
    if format == "ascii":
        mesh.text = True
    elif format == "binary_le" or (format == "binary" and sys.byteorder == "little"):
        mesh.text = False
        mesh.byte_order = "<"
    else:
        mesh.text = False
        mesh.byte_order = ">"



if __name__ == "__main__":
    try:
        args = parse_args()
        with open(args.input_file, "rb") as f:
            mesh = plyfile.PlyData.read(f)
        for v in mesh["vertex"]:
            t = numpy.array([(v["x"]), (v["y"]), (v["z"]) ,(1)])
            t = args.T @ t
            v["x"] = t[0]
            v["y"] = t[1]
            v["z"] = t[2]
        set_mesh_format(mesh, args.format)
        mesh.write(args.output_file)
    except KeyboardInterrupt:
        pass

