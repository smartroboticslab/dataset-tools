#ifndef SCENE2RAW_SHARED_H
#define SCENE2RAW_SHARED_H

typedef unsigned short int ushort;

struct CameraCalibration
{
    CameraCalibration(float u0, float v0, float focal_x, float focal_y) : u0(u0), v0(v0), focal_x(focal_x),
                                                                          focal_y(focal_y) {}

    float u0;
    float v0;
    float focal_x;
    float focal_y;
};

struct uchar3 {
    unsigned char x, y, z;
};
struct uchar4 {
    unsigned char x, y, z, w;
};

struct uint2 {
    unsigned int x, y;
};

void distortDepthFrame(ushort *depthMap, int h, int w, const CameraCalibration &cal);

inline uchar3 make_uchar3(unsigned char x, unsigned char y, unsigned char z)
{
    uchar3 val;
    val.x = x;
    val.y = y;
    val.z = z;
    return val;
}

inline uchar4 make_uchar4(unsigned char x, unsigned char y, unsigned char z, unsigned char w)
{
    uchar4 val;
    val.x = x;
    val.y = y;
    val.z = z;
    val.w = w;
    return val;
}

inline uint2 make_uint2(unsigned int x, unsigned int y)
{
    uint2 val;
    val.x = x;
    val.y = y;
    return val;
}

void distortDepthFrame(ushort *depthMap, int h, int w, const CameraCalibration &cal)
{
  for (int v = 0; v < h; v++) {
    for (int u = 0; u < w; u++) {
      double u_u0_by_fx = (u - cal.u0) / cal.focal_x;
      double v_v0_by_fy = (v - cal.v0) / cal.focal_y;

      depthMap[u + v * w] = depthMap[u + v * w] / std::sqrt(u_u0_by_fx * u_u0_by_fx
          + v_v0_by_fy * v_v0_by_fy
          + 1);
    }
  }
}

#endif //SCENE2RAW_SHARED_H
