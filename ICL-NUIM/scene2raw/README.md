# SLAMBench 1.0 Dataset generation tool

`scene2raw` is a self-contained tool that generates SLAMBench compatible raw files for ICL-NUIM.

## Usage

``` sh
./scene2raw /path/to/depth/data/ /path/to/output/file.raw
```
