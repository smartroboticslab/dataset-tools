#!/bin/bash
# SPDX-FileCopyrightText: 2019-2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2019-2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# Set up a Python virtualenv with the YOLACT implementation from:
#   https://github.com/dbolya/yolact

# Select operations to perform. 1 enables, 0 disables.
# Install CUDA.
setup_cuda=0
# Install Miniconda3
setup_miniconda=0
# Create a Miniconda environment with the required dependencies.
setup_yolact_env=1
# Clone the repository and setup.
setup_yolact=0
# Clone the SRL YOLACT repository and setup.
setup_yolact_srl=1



# Set Bash settings.
set -euo pipefail
IFS=$'\n\t'

# Setup CUDA ###################################################################
cuda_url='https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.1.105-1_amd64.deb'
cuda_key='https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub'
cuda_deb="/tmp/$(basename "$cuda_url")"

if [[ "$setup_cuda" -eq 1 ]] ; then
	echo
	echo "Installing CUDA #####################################################"
	echo 'Update the cuda_url and cuda_key variables with the latest CUDA (deb network) version from here:'
	echo 'https://developer.nvidia.com/cuda-downloads'
	echo 'Also see installation guide from here:'
	echo 'https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html'
	echo 'Press ENTER to continue'
	read

	wget -O "$cuda_deb" "$cuda_url"
	sudo dpkg -i "$cuda_deb"
	sudo apt-key adv --fetch-keys "$cuda_key"
	sudo apt update
	sudo apt install cuda

	echo 'Add the following command to your .bashrc:'
	echo 'export PATH=/usr/local/cuda-10.1/bin:/usr/local/cuda-10.1/NsightCompute-2019.1${PATH:+:${PATH}}'
	echo 'Reboot the computer before continuing with the setup'
	echo 'Press ENTER to quit setup'
	read
	exit 0
else
	echo
	echo "Skipping CUDA installation ##########################################"
fi



# Setup Miniconda ##############################################################
miniconda_url='https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh'
miniconda_instaler="/tmp/$(basename "$miniconda_url")"
miniconda_dir="$HOME/Documents/miniconda3"

if [[ "$setup_miniconda" -eq 1 ]] ; then
	echo
	echo "Installing and updating Miniconda ###################################"

	# Download and install.
	wget -O "$miniconda_instaler" "$miniconda_url"
	chmod +x "$miniconda_instaler"
	"$miniconda_instaler" -p "$miniconda_dir"
	rm -f "$miniconda_instaler"

	# Update conda.
	source "$miniconda_dir/bin/activate"
	conda update -y -n base -c defaults conda
	conda deactivate
else
	echo
	echo "Skipping Miniconda installation #####################################"
fi



# Setup YOLACT Miniconda environment ###########################################
yolact_env='yolact'

if [[ "$setup_yolact_env" -eq 1 ]] ; then
	echo
	echo "Seting up YOLACT environment ########################################"

	source "$miniconda_dir/bin/activate"
	conda create --name "$yolact_env"

	conda activate "$yolact_env"
	conda install -y -c pytorch pytorch-nightly torchvision cudatoolkit
	pip install --upgrade pip
	pip install cython
	pip install opencv-python pillow pycocotools matplotlib
	conda deactivate

	conda deactivate
else
	echo
	echo "Skipping YOLACT environment setup ###################################"
fi



# Setup YOLACT #################################################################
repo_dir="$miniconda_dir/envs/$yolact_env/source"
repo_url_upstream='https://github.com/dbolya/yolact.git'
repo_url_srl='git@bitbucket.org:smartroboticslab/yolact.git'
# Set the appropriate repository URL
if [[ "$setup_yolact_srl" -eq 1 ]] ; then
	repo_url="$repo_url_srl"
else
	repo_url="$repo_url_upstream"
fi

if [[ "$setup_yolact" -eq 1 ]] || [[ "$setup_yolact_srl" -eq 1 ]] ; then
	echo
	echo "Seting up YOLACT ####################################################"

	# Clone required repositories.
	mkdir -p "$repo_dir"
	git clone --depth 1 "$repo_url" "$repo_dir/yolact"

	# Download weights
	mkdir -p "$repo_dir/yolact/weights"
	# wget does not download the correct file
	#wget --no-check-certificate --recursive 'https://drive.google.com/uc?export=download&id=1yp7ZbbDwvMiFJEq4ptVKTYTI2VeRDXl0' -O "$repo_dir/yolact/weights/yolact_resnet50_54_800000.pth"
	echo "Manually download the weights and place them in $repo_dir/yolact/weights"

	# Create hard link to the script. Symbolic links will not work correctly.
	ln "./yolact_on_dataset.py" "$repo_dir/yolact"
else
	echo
	echo "Skipping YOLACT setup ###############################################"
fi

