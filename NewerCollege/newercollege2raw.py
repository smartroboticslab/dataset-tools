#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause
#
# pcd2raw.py - Convert a Newer College dataset to .raw
# Combine the .pcd LIDAR scan files in a Newer College dataset into the
# SLAMBench v1.0 .raw format.

from glob import glob
from math import sqrt
from os.path import isdir
from struct import pack
from sys import argv
from time import time
from typing import BinaryIO

from pcd2pgm import *


class Image:
    """Generic image stored in row major format."""
    def __init__(
            self,
            width: int = 0,
            height: int = 0,
            pixel_size: int = 4
    ) -> None:
        self.width = width
        self.height = height
        self.pixel_size = pixel_size
        self.data = bytearray([0] * self.pixel_size * self.width * self.height)

    def size(
            self
    ) -> int:
        """Size of the image data in bytes."""
        return self.pixel_size * self.width * self.height

    def write(
            self,
            file: BinaryIO
    ) -> None:
        """Write the image dimensions and raw data."""
        # Pack into 4-byte ints
        wb = pack("<I", self.width)
        hb = pack("<I", self.height)
        file.write(wb)
        file.write(hb)
        file.write(self.data)


def write_depthimage(
        depth: DepthImage,
        file: BinaryIO
) -> None:
    # Pack the dimensions into 4-byte ints and write
    wb = pack("<I", depth.width)
    hb = pack("<I", depth.height)
    file.write(wb)
    file.write(hb)
    # Convert each pixel to the proper binary format
    for i in range(len(depth.data)):
        val = depth.data[i]
        val_binary = pack("<H", val)
        file.write(val_binary)


if __name__ == "__main__":
    # Parse input arguments
    assert len(argv) >= 2, "Usage: " + argv[0] + " INPUT_DIR [OUTPUT_RAW]"
    input_dir = argv[1]
    assert isdir(input_dir), input_dir + " is not a directory"
    if len(argv) == 2:
        raw_filename = input_dir + "/scene.raw"
    else:
        raw_filename = argv[2]

    frames_written = 0
    # Open output file for writing
    with open(raw_filename, "wb") as f:
        # Get all PCL files in the input directory and sort by name
        pcl_filenames = glob(input_dir + "/*.pcd")
        pcl_filenames.sort()
        total_frames = len(pcl_filenames)
        # Iterate over all PCL files
        start_t = time()
        for pcl_filename in pcl_filenames:
            pc = load_pcl(pcl_filename)
            reorder_scan(pc)
            depth = pointcloud_to_depth(pc)
            write_depthimage(depth, f)
            # Write an empty RGBA image
            rgba = Image(depth.width, depth.height)
            rgba.write(f)
            # Print progress
            frames_written += 1
            iter_t = time()
            eta = (iter_t - start_t) * (total_frames - frames_written) / frames_written
            print("Frame " + str(frames_written) + "/" + str(total_frames)
                    + " (" + str(int(100 * frames_written / total_frames)) + " %)"
                    + "   ETA " + str(int(eta)) + " s          \r", end="")

