#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause
#
# pcd2pgm.py - Convert .pcd file to .pgm
# Convert a single .pcd file produced by an Ouster OS-1-64 LIDAR into a 16-bit
# PGM image.
# WARNING: 16-bits are not enough to represent measurements further than 65.535
# meters. In those cases the measurements are clamped to 65.535 meters.


from math import sqrt
from sys import argv
from typing import List, Union


class PointCloud:
    """Structured point cloud with data in row major format."""
    def __init__(
            self,
            width: int = 0,
            height: int = 0,
            data: List[List[float]] = None
    ) -> None:
        self.width = width
        self.height = height
        if data is None:
            self.data: List[List[float]] = []
        else:
            self.data = data


class DepthImage:
    """Depth image with 16-bit pixels in row major format."""
    def __init__(
            self,
            width: int = 0,
            height: int = 0,
            data: List[int] = None
    ) -> None:
        self.width = width
        self.height = height
        if data is None:
            self.data: List[int] = []
        else:
            self.data = data


def load_pcl(
        filename: str
) -> PointCloud:
    """Load a PCL 0.7 pointcloud from a file."""
    pc = PointCloud()
    with open(filename, "r") as f:
        # Read the .pcd file line-by-line
        for line in f:
            line = line.rstrip()
            # Skip empty lines
            if not line:
                continue
            # Skip comment lines
            if line.startswith("#"):
                continue
            # Check for correct version
            if line.startswith("VERSION"):
                assert line.endswith("0.7"), \
                        "Error reading PCL file: expected VERSION 0.7"
                continue
            # Check for correct data type
            if line.startswith("DATA"):
                assert line.endswith("ascii"), \
                        "Error reading PCL file: expected DATA ascii"
                continue
            # Skip lines that are not used
            if (       line.startswith("FIELDS")
                    or line.startswith("SIZE")
                    or line.startswith("TYPE")
                    or line.startswith("COUNT")
                    or line.startswith("VIEWPOINT")
                    or line.startswith("POINTS")):
                continue
            # Read width
            if line.startswith("WIDTH"):
                pc.width = int(line.rsplit(' ', 1)[-1])
                continue
            # Read height
            if line.startswith("HEIGHT"):
                pc.height = int(line.rsplit(' ', 1)[-1])
                continue
            # Read 3D point
            point = [float(x) for x in line.split(' ')]
            assert len(point) >= 3, \
                    "Error reading PCL file: expected at least 3 point coordinates"
            pc.data.append(point)
    return pc


def reorder_scan(
        d: PointCloud
) -> None:
    """Reorder a structured point cloud from an Ouster OS-1-64 LIDAR.

    In order to produce a singe 360 scan, the Ouster OS-1-64 LIDAR performs 16
    scans and combines the results. However the raw scans returned by the LIDAR
    not sorted by azimuth angle. This function performs the appropriate
    measurement reordering.
    """
    px_offset = [0, 6, 12, 18] * 16
    reordered_indices = []
    for y in range(d.height):
        for x in range(d.width):
            vv = (x + px_offset[y]) % d.width;
            index = vv * d.height + y;
            reordered_indices.append(index)
    d.data = [d.data[i] for i in reordered_indices]


def pointcloud_to_depth(
        pc: PointCloud
) -> DepthImage:
    """Convert a structured point cloud to a depth image."""
    # Initialize depth image
    depth = DepthImage(pc.width, pc.height)
    # Convert each 3D point to a pixel
    for point in pc.data:
        # Compute the distance of the 3D point from the origin
        depth_val = int(1000 * sqrt(sum([x**2 for x in point])))
        # Make depth values that do not fit in 16 bits invalid
        if (depth_val > 2**16 - 1):
            depth_val = 0
        depth.data.append(depth_val)
    return depth


def save_pgm(
        depth: DepthImage,
        filename: str
) -> None:
    """Save a 16-bit depth image in PGM format."""
    with open(filename, "w") as f:
        # Write the PGM header assuming 16-bit depth values
        f.write("P2\n"
                + str(depth.width) + " " + str(depth.height) + "\n"
                + str(2**16 - 1) + "\n")
        # Iterate over each image pixel
        for i in range(len(depth.data)):
            val = depth.data[i]
            f.write(str(val))
            # Add a newline at the end of each image row and a space between
            # intermediate pixels
            if (i % depth.width) == 0:
                f.write("\n")
            else:
                f.write(" ")


if __name__ == "__main__":
    # Parse input arguments
    assert len(argv) >= 3, "Usage: " + argv[0] + " INPUT_PCD OUTPUT_PGM"
    input_pcd = argv[1]
    output_pgm = argv[2]

    pc = load_pcl(input_pcd)
    reorder_scan(pc)
    depth = pointcloud_to_depth(pc)
    save_pgm(depth, output_pgm)

