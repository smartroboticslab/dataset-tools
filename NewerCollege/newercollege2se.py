#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause
#
# newercollege2se.py - Convert a Newer College dataset to a format that supereight understands

import argparse
import csv
import glob
import os
import re
import shutil
import sys

from typing import List



_before_timestamp_pattern = re.compile('(.*cloud_\d+)_.*')
_id_pattern = re.compile('(.*cloud_)(\d+)')
_num_id_digits = 5



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Convert a Newer College dataset to a format that supereight understands.')
    parser.add_argument('indir', metavar='INDIR',
            help='The directory containing the dataset.')
    parser.add_argument('outdir', metavar='OUTDIR', nargs='?',
            help='The converted dataset will be placed inside the OUTDIR directory. OUTDIR will be'
            'created if it doesn\'t already exist.')
    parser.add_argument('-i', '--in-place', action='store_true',
            help='Convert the dataset in-placing instead of placing the converted files into OUTDIR.')
    args = parser.parse_args()
    if not os.path.isdir(args.indir):
        printerr('INDIR must be an existing directory')
        sys.exit(2)
    if not args.indir.endswith('/'):
        args.indir = args.indir + '/'
    if args.outdir is not None:
        if os.path.isfile(args.outdir):
            printerr('OUTDIR must be a directory, not a file')
            sys.exit(2)
        if args.in_place:
            printerr('OUTDIR and -i/--in-place are mutually exclusive.')
            sys.exit(2)
        if not args.outdir.endswith('/'):
            args.outdir = args.outdir + '/'
    else:
        if not args.in_place:
            printerr('OUTDIR or -i/--in-place must be supplied.')
            sys.exit(2)
    return args



def prepend_zeros(s: str, desired_len: int) -> str:
    new_s = s
    for _ in range(desired_len - len(s)):
        new_s = '0' + new_s
    return new_s



def convert_filename(filename: str) -> str:
    new_filename = filename
    # Remove the extension
    if new_filename.endswith('.pcd'):
        new_filename = new_filename[:-4]
    # Remove the timestamp
    new_filename = _before_timestamp_pattern.sub('\\1', new_filename, 1)
    # Find the cloud ID
    m = _id_pattern.match(new_filename)
    id_str = m.group(2)
    # Add leading zeros if needed
    id_str = prepend_zeros(id_str, _num_id_digits)
    # Use the expanded ID
    new_filename = m.expand('\\1') + id_str
    # Re-add the extension
    return new_filename + '.pcd'



def convert_ground_truth(dir: str) -> str:
    old_gt = glob.glob(dir + '*.csv')
    if not old_gt:
        return None, None
    old_gt = old_gt[0]
    new_gt = dir + 'association.txt'
    with open(old_gt) as inf, open(new_gt, 'w') as outf:
        reader = csv.reader(inf)
        outf.write('# ID timestamp tx ty tz qx qy qz qw\n')
        for line in reader:
            if line[0].startswith('#'):
                continue
            if len(line) != 10:
                return None, None
            print(line)
            line[0] = prepend_zeros(line[0], _num_id_digits)
            line[1] = prepend_zeros(line[1], 10)
            line[2] = prepend_zeros(line[2], 6)
            line[1] = line[1] + '.' + line[2]
            del line[2]
            outf.write(' '.join(line) + '\n')
    return old_gt, new_gt



def get_input_clouds(dir: str) -> List[str]:
    # The returned paths are relative to dir.
    return glob.glob(dir + '/cloud_*.pcd')



def get_output_clouds(input_clouds: List[str], args) -> List[str]:
    if args.in_place:
        return [convert_filename(x) for x in input_clouds]
    else:
        return [convert_filename(x).replace(args.indir, args.outdir) for x in input_clouds]



if __name__ == "__main__":
    try:
        args = parse_args()

        input_clouds = get_input_clouds(args.indir)
        if not input_clouds:
            printerr('No files matching the pattern cloud_*.pcd found in', args.indir)
            sys.exit(1)

        output_clouds = get_output_clouds(input_clouds, args)

        if args.outdir is not None:
            try:
                os.mkdir(args.outdir)
            except FileExistsError:
                pass

        # Convert the point clouds
        for i, o in zip(input_clouds, output_clouds):
            if i != o:
                if args.in_place:
                    os.rename(i, o)
                else:
                    shutil.copyfile(i, o)

        # Convert the ground truth
        old_gt, new_gt = convert_ground_truth(args.indir)
        if old_gt and new_gt:
            if args.in_place:
                os.replace(new_gt, old_gt)
            else:
                os.rename(new_gt, args.outdir + 'association.txt')
    except KeyboardInterrupt:
        pass

