#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause
#
# newercollege2tum.py - Convert a Newer College dataset to the TUM format
# WARNING: 16-bits as used by TUM PNGs are not enough to represent
# measurements further than 65.535 meters. In those cases the measurements
# are clamped to 65.535 meters.

import argparse
import csv
import glob
import os
import subprocess
import sys
import time
from typing import List, Union

from pcd2pgm import *


class Frame:
    """Information regarding a single frame of a TUM dataset."""
    def __init__(
            self,
            id: int = 0,
            sec: int = 0,
            nsec: int = 0,
            position: List[float] = [0, 0, 0],
            orientation: List[float] = [0, 0, 0, 1]
    ) -> None:
        self.id = id
        self.sec = sec
        self.nsec = nsec
        self.position = position
        self.orientation = orientation

    def timestamp(self) -> float:
        """Return the timestamp as float seconds."""
        return float(self.sec) + float(self.nsec) * 10**-6

    def tum_depth_filename(self) -> str:
        """Return the filename of the respective TUM depth image."""
        return "depth/{:.6f}.png".format(self.timestamp())

    def tum_rgb_filename(self) -> str:
        """Return the filename of the respective TUM RGB image."""
        return "rgb/{:.6f}.png".format(self.timestamp())

    def to_tum_groundtruth_format(self) -> str:
        """Return a string in the format of TUM groundtruth.txt files."""
        return ("{:.4f} {:.4f} {:.4f} {:.4f} "
                "{:.4f} {:.4f} {:.4f} {:.4f}").format(
                self.timestamp(),
                self.position[0], self.position[1], self.position[2],
                self.orientation[0], self.orientation[1], self.orientation[2],
                self.orientation[3])

    def to_tum_depth_format(self) -> str:
        """Return a string in the format of TUM depth.txt files."""
        return "{:.6f} {}".format(self.timestamp(), self.tum_depth_filename())

    def to_tum_rgb_format(self) -> str:
        """Return a string in the format of TUM rgb.txt files."""
        return "{:.6f} {}".format(self.timestamp(), self.tum_rgb_filename())

    def to_supereight_format(self) -> str:
        """Return a string in the format of supereight groundtruth files."""
        return ("{:.6f} {} {} {:.4f} {:.4f} {:.4f} "
                "{:.4f} {:.4f} {:.4f} {:.4f}").format(
                self.timestamp(),
                self.tum_rgb_filename(), self.tum_depth_filename(),
                self.position[0], self.position[1], self.position[2],
                self.orientation[0], self.orientation[1], self.orientation[2],
                self.orientation[3])


def progress_str(
        current: int,
        total: int,
        start_time: float
) -> str:
    """Return a string showing the progress and ETA of a task."""
    current_time = time.time()
    eta = (current_time - start_time) * (total - current) / current
    percentage = 100 * current / total
    return "Frame {:6d}/{:<6d} ({:3.0f} %)   ETA {:.0f} s".format(
        current, total, percentage, eta)


def get_pcd_filename(dir: str, frame: Frame) -> Union[str, None]:
    """Return the absolute path to the .pcd file in dir matching frame."""
    # Need to perform glob matching because the .csv contains a shorter
    # timestamp than the files
    pcd_fname_pattern = dir + "/cloud_{:04d}_{:d}_{:d}*.pcd".format(
            frame.id, frame.sec, frame.nsec)
    # Remove the last timestamp digit which could have been rounded up
    pcd_fname_pattern = pcd_fname_pattern[:-6] + pcd_fname_pattern[-5:]
    pcd_fname = glob.glob(pcd_fname_pattern)
    if pcd_fname:
        return pcd_fname[0]
    else:
        return None


def parse_arguments():
    parser = argparse.ArgumentParser(
            description='Convert a Newer College datatet to the TUM format')
    parser.add_argument(
            'input_dir',
            type=str,
            metavar='INPUT_DIR',
            help='the directory containing the Newer College dataset files')
    parser.add_argument(
            'output_dir',
            nargs='?',
            type=str,
            metavar='OUTPUT_DIR',
            default=None,
            help='the directory where the TUM files will be written')
    args = parser.parse_args()
    if not os.path.isdir(args.input_dir):
        print("Error: " + args.input_dir + " is not a directory")
        sys.exit(1)
    args.input_dir = args.input_dir.rstrip("/")
    if args.output_dir is None:
        args.output_dir = args.input_dir + "_freiburg1"
    return args


def parse_newcollege_csv(
        filename: str
) -> List[Frame]:
    frames = []
    dataset_dir = os.path.dirname(filename)
    with open(filename, "rt", newline="") as f:
        r = csv.reader(f)
        for row in r:
            # Skip comment lines
            if row[0].startswith("#"):
                continue
            # Read and generate the frame information
            id = int(row[0])
            sec = int(row[1])
            nsec = int(row[2])
            position = [float(x) for x in row[3:6]]
            orientation = [float(x) for x in row[6:10]]
            fr = Frame(id, sec, nsec, position, orientation)
            frames.append(fr)
    return frames


def write_tum_txt(dir: str, frames: List[Frame]) -> None:
    # Create the dataset directory if it doesn't exist
    if not os.path.exists(dir):
        os.mkdir(dir)
    # Create depth.txt, rgb.txt and groundtruth.txt
    with    open(dir + "/depth.txt", "wt") as fd, \
            open(dir + "/rgb.txt", "wt") as fr, \
            open(dir + "/groundtruth.txt", "wt") as fg:
        # Write the headers
        fd.write("# depth maps\n# timestamp filename\n")
        fr.write("# color images\n# timestamp filename\n")
        fg.write("# ground truth trajectory\n# timestamp tx ty tz qx qy qz qw\n")
        # Write frame data
        for frame in frames:
            fd.write(frame.to_tum_depth_format() + "\n")
            fr.write(frame.to_tum_rgb_format() + "\n")
            fg.write(frame.to_tum_groundtruth_format() + "\n")


def write_supereight_txt(dir: str, frames: List[Frame]) -> None:
    # Create the dataset directory if it doesn't exist
    if not os.path.exists(dir):
        os.mkdir(dir)
    # Create depth.txt, rgb.txt and groundtruth.txt
    with open(dir + "/timings.assoc_extended.txt", "wt") as fs:
        # Write the header
        fs.write("# association of rgb images, depth images and ground truth measurements\n"
                "# timestamp rgb_filename depth_filename tx ty tz qx qy qz qw\n")
        # Write frame data
        for frame in frames:
            fs.write(frame.to_supereight_format() + "\n")


def generate_depth(
        output_dir: str,
        input_dir: str,
        frames: List[Frame]
) -> None:
    # Create the dataset directory if it doesn't exist
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    # Create the depth directory if it doesn't exist
    depth_dir = output_dir + "/depth"
    if not os.path.exists(depth_dir):
        os.mkdir(depth_dir)
    # Iterate over all .pcd files
    print("Converting .pcl files to .png")
    start_t = time.time()
    for i in range(len(frames)):
        frame = frames[i]
        pcd_fname = get_pcd_filename(input_dir, frame)
        if pcd_fname is None:
            print("Error: could not find file for frame\n  "
                    + frame.to_supereight_format())
            sys.exit(1)
        # Load the .pcd and convert to depth
        pc = load_pcl(pcd_fname)
        reorder_scan(pc)
        depth = pointcloud_to_depth(pc)
        # Scale to the TUM dataset specification
        for j in range(len(depth.data)):
            depth.data[j] = 5 * depth.data[j]
            # Make depth values that do not fit in 16 bits invalid
            if depth.data[j] > 2**16 - 1:
                depth.data[j] = 0
        # Save the depth image as .pgm and convert to .png in-place
        depth_fname = output_dir + "/" + frame.tum_depth_filename()
        depth_fname_pgm = depth_fname.replace(".png", ".pgm")
        save_pgm(depth, depth_fname_pgm)
        subprocess.run(["convert", depth_fname_pgm, depth_fname])
        os.remove(depth_fname_pgm)
        # Print progress
        print(progress_str(i + 1, len(frames), start_t) + "        \r", end="")
    print()


def generate_rgb(
        output_dir: str,
        input_dir: str,
        frames: List[Frame]
) -> None:
    # Create the dataset directory if it doesn't exist
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    # Create the rgb directory if it doesn't exist
    rgb_dir = output_dir + "/rgb"
    if not os.path.exists(rgb_dir):
        os.mkdir(rgb_dir)
    # Load the first .pcd file to get the scan dimensions
    pcd_fname = get_pcd_filename(input_dir, frames[0])
    if pcd_fname is None:
        print("Error: could not find file for frame\n  "
                + frames[0].to_supereight_format())
        sys.exit(1)
    pc = load_pcl(pcd_fname)
    width = pc.width
    height = pc.height
    # Generate a black PNG image corresponding to the first .pcd file
    first_rgb_fname = output_dir + "/" + frames[0].tum_rgb_filename()
    subprocess.run(["convert", "-size", "{}x{}".format(width, height),
            "-colorspace", "RGB", "canvas:black", first_rgb_fname])
    # Iterate over all frames except the first
    print("Generating RGB images")
    start_t = time.time()
    for i in range(1, len(frames)):
        frame = frames[i]
        # Create the RGB image symlink overwriting existing symlinks
        rgb_fname = output_dir + "/" + frame.tum_rgb_filename()
        if os.path.islink(rgb_fname):
            os.remove(rgb_fname)
        os.symlink(first_rgb_fname, rgb_fname)
        # Print progress
        print(progress_str(i + 1, len(frames), start_t) + "        \r", end="")
    print()


if __name__ == "__main__":
    # Parse input arguments
    args = parse_arguments()

    # Load the groundtruth .csv file
    csv_filename = args.input_dir + "/registered_poses_lidar.csv"
    frames = parse_newcollege_csv(csv_filename)

    # Write the TUM and supereight text files
    write_tum_txt(args.output_dir, frames)
    write_supereight_txt(args.output_dir, frames)

    # Convert each .pcd file to a depth image
    generate_depth(args.output_dir, args.input_dir, frames)

    # Generate a black colour image for each depth image
    generate_rgb(args.output_dir, args.input_dir, frames)

