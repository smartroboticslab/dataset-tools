#!/bin/sh
# Create a single ZIP archive from a Newer College Dataset sequence ensuring
# all the files inside it are ordered by timestamp. The dataset directory is
# assumed to look like this:
#
# 01_short_experiment_2020_raw/
# ├── cloud_1583836591_182590976.pcd
# ├── cloud_1583836591_282592512.pcd
# ├── ...
# ├── cloud_1583838121_056272640.pcd
# ├── cloud_1583838121_156355584.pcd
# └── registered_poses.csv
#
# The resulting ZIP archive can be split into several 2 GiB ZIP archives by
# running:
#
# zipsplit -sn 2147483648 FILE
#
# Using the first N of the smaller archives will result in using only the
# beginning of the dataset since the point clouds are sorted by their
# timestamp.

set -eu

if [ "$#" -ne 2 ]
then
	printf 'Usage: %s DIR ARCHIVE\n' "${0##*/}" >&2
	exit 2
fi

# Change into the parent of the sequence directory so that the ZIP is
# guaranteed to contain just the sequence directory and not any parent
# directories.
cd "$1"/.. || exit 1
# Put all non-PCD files (e.g. ground turth) first in the archive so that they
# are in the first sub-archive if the archive is split into multiple smaller
# ones. Then add all PCD files sorted by name so that the first N archives can
# be used to process only the beginning of the dataset.
# shellcheck disable=SC2046
zip "$2" $(find "$(basename "$1")" -type f ! -name '*.pcd' | sort
	find "$(basename "$1")" -type f -name '*.pcd' | sort)
