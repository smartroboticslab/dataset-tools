# NewerCollege

Utilities for the [Newer College
dataset](https://ori-drs.github.io/newer-college-dataset/).

**Warning**: These utilities are meant for debugging and not serious use. They
convert LIDAR measurements from `float` to `uint16_t` which makes it impossible
to represent measurements further away than 65 metres (without scaling at
least).



## License

Copyright © 2020 Smart Robotics Lab, Imperial College London

Distributed under the BSD 3-Clause license.

