#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause
#
# newercollege_map_params.py - compute map dimensions and t_MW for a Newer College dataset

import argparse
import csv
import glob
import numpy as np
import os
import sys
import time

from math import inf
from typing import List, NamedTuple, Tuple



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



class GroundTruth:
    def __init__(self, filename: str) -> None:
        # Count the scans in the ground truth file
        num_scans = 0
        with open(filename) as f:
            for l in f:
                if l[0] != '#':
                    num_scans += 1
        # Preallocate arrays
        self.ids = []
        self.pos = np.zeros((num_scans, 3), np.float32)
        # Read all lines in the CSV file except comments
        with open(filename) as f:
            reader = csv.reader(f)
            idx = 0
            for line in reader:
                if line[0][0] != '#':
                    self.ids.append(line[0])
                    self.pos[idx,:] = np.array([float(line[4]), float(line[5]), float(line[6])], np.float32)
                    idx += 1

    def __len__(self) -> int:
        return len(self.ids)

    def __iter__(self):
        return ((id, p) for id, p in zip(self.ids, self.pos))



class Scan:
    def __init__(self, filename: str, position: np.ndarray) -> None:
        # Count the scan points in the PCD file and the lines to skip
        num_points = 0
        skip_idx = -1
        with open(filename) as f:
            for i, l in enumerate(f):
                if not l[0].isupper() and l[0] != '#':
                    num_points += 1
                else:
                    skip_idx = i
        # Preallocate arrays
        self.pos = position
        self.points = np.zeros((num_points, 3), np.float32)
        # Read all scan points
        idx = 0
        with open(filename) as f:
            for line_idx, line in enumerate(f):
                if line_idx > skip_idx:
                    c = [float(x) for x in line.split(' ')]
                    self.points[idx,:] = np.array(c, np.float32)
                    idx += 1

    def min_point(self) -> np.ndarray:
        return np.amin(self.points, axis=0) + self.pos

    def max_point(self) -> np.ndarray:
        return np.amax(self.points, axis=0) + self.pos

    def filter_far_plane(self, far_plane: float) -> None:
        del_idx = []
        for idx, p in enumerate(self.points):
            if np.linalg.norm(p) > far_plane:
                del_idx.append(idx)
        np.delete(self.points, del_idx, axis=0)



class AABB:
    def __init__(self) -> None:
        self.min_pos = np.array([ inf,  inf,  inf], np.float32)
        self.max_pos = np.array([-inf, -inf, -inf], np.float32)

    def __str__(self) -> str:
        r = self.range()
        header = '  {:>10} {:>10} {:>10}'.format('min (m)', 'max (m)', 'range (m)')
        x_str = 'x {: 10.3f} {: 10.3f} {: 10.3f}'.format(self.min_pos[0], self.max_pos[0], r[0])
        y_str = 'y {: 10.3f} {: 10.3f} {: 10.3f}'.format(self.min_pos[1], self.max_pos[1], r[1])
        z_str = 'z {: 10.3f} {: 10.3f} {: 10.3f}'.format(self.min_pos[2], self.max_pos[2], r[2])
        return '\n'.join([header, x_str, y_str, z_str])

    def merge_point(self, p: np.ndarray) -> None:
        self.min_pos = np.minimum(self.min_pos, p)
        self.max_pos = np.maximum(self.max_pos, p)

    def merge_scan(self, s: Scan) -> None:
        self.merge_point(s.min_point())
        self.merge_point(s.max_point())

    def range(self) -> np.ndarray:
        return self.max_pos - self.min_pos



def get_dataset_filenames(dir: str) -> Tuple[str, List[str]]:
    csv_filenames = glob.glob(dir + '/*.csv')
    if csv_filenames:
        gt_filename = csv_filenames[0]
    else:
        gt_filename = ''
    scan_filenames = glob.glob(dir + "/*.pcd")
    scan_filenames.sort()
    return gt_filename, scan_filenames



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Compute map dimensions and t_MW for a Newer College dataset.')
    parser.add_argument('input_dir', metavar='DIRECTORY',
            help='The directory containing the dataset.')
    parser.add_argument('-r', '--rate', metavar='N', type=int, default=10,
            help='Process every N scans.')
    parser.add_argument('-f', '--far-plane', metavar='DIST', type=float,
            help='Discard scan points further than DIST metres.')
    args = parser.parse_args()
    args.gt_filename, args.scan_filenames = get_dataset_filenames(args.input_dir)
    if args.rate <= 0:
        printerr('-r/--rate must be positive')
        sys.exit(2)
    if not args.gt_filename:
        printerr('Could not find any CSV files in ' + args.input_dir)
        sys.exit(2)
    if not args.scan_filenames:
        printerr('Could not find any PCD files in ' + args.input_dir)
        sys.exit(2)
    return args



if __name__ == "__main__":
    try:
        # Parse input arguments
        config = parse_args()
        # Load the ground truth
        gt = GroundTruth(config.gt_filename)
        # Load each scan and compute the map AABB
        aabb = AABB()
        scans_read = 0
        start_time = time.time()
        for idx, (id, pos) in enumerate(gt):
            if idx % config.rate == 0:
                if id in config.scan_filenames[idx]:
                    scan = Scan(config.scan_filenames[idx], pos)
                    if config.far_plane is not None:
                        scan.filter_far_plane(config.far_plane)
                    aabb.merge_scan(scan)
                    scans_read += 1
                    print('Read ' + config.scan_filenames[idx])
                else:
                    printerr('Could not find PCD file containing ID ' + id)
        end_time = time.time() - start_time
        # Compute the map dimensions
        map_dim = float(round(max(aabb.range()) + 2.0))
        # Compute the relative offset of the initial position in the map frame
        init_pos_W = gt.pos[0,:]
        t_MW_factor = np.divide(init_pos_W - aabb.min_pos, aabb.range())
        t_MW = map_dim * t_MW_factor
        # Compute voxel dimensions for various map resolutions
        res = [256, 512, 1024, 2048, 4096]
        voxel_dim = [map_dim / x for x in res]
        # Print results
        print('Processed {} PCD files in {:.2f} s'.format(scans_read, end_time))
        print('Average {:.4f} s per file'.format(end_time / scans_read))
        print('Map AABB -----------------------------------------')
        print(aabb)
        print('Map parameters -----------------------------------')
        print('t_MW factor:  {:1.5f}  {:1.5f}  {:1.5f}'.format(*t_MW_factor))
        print('t_MW:         {:1.5f}  {:1.5f}  {:1.5f}'.format(*t_MW))
        print('Map dim (m)     ' + str(map_dim))
        print('Map res:        {:7}  {:7}  {:7}  {:7}  {:7}'.format(*res))
        print('Voxel dim (m):  {:7.4f}  {:7.4f}  {:7.4f}  {:7.4f}  {:7.4f}'.format(*voxel_dim))
    except KeyboardInterrupt:
        pass

