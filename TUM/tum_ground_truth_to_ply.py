#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# tum_ground_truth_to_ply.py
# Generate a PLY file containing the positions from a TUM ground truth file.
#
# Generate a file named path.ply containing every 10 positions from
# /path/to/groundtruth.txt and shown in green in the PLY file
#     tum_ground_truth_to_ply.py -o path.ply -r 10 -c 00FF00 /path/to/groundtruth.txt
#
# Show detailed program help
#     tum_ground_truth_to_ply.py --help

import argparse
import fileinput
import numpy as np
import os
import quaternion
import sys



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Generate a PLY file '
            'containing the positions from a TUM ground truth file.')
    parser.add_argument('infile', metavar='INPUT_FILE', nargs='?', default='-',
            help='With no INPUT_FILE, or when INPUT_FILE is -, read standard input.')
    parser.add_argument('outfile', metavar='OUTPUT_FILE', nargs='?',
            help='With no OUTPUT_FILE write to standard output.')
    parser.add_argument('-r', '--pose-rate', metavar='N', type=int, default=1,
            help='Use every N ground truth poses.')
    parser.add_argument('-c', '--colour', metavar='RRGGBB', default='FF0000',
            help='The colour to use for the trajectory positions in hexadecimal.')
    args = parser.parse_args()
    if args.pose_rate <= 0:
        printerr('The pose rate must be a positive integer')
        sys.exit(2)
    return args



def hex_to_rgb(colour_hex: str):
    if len(colour_hex) != 6:
        return 0, 0, 0
    red = int(colour_hex[0:2], 16)
    green = int(colour_hex[2:4], 16)
    blue = int(colour_hex[4:6], 16)
    return red, green, blue



def read_poses(filename: str, rate: int):
    tx = []
    ty = []
    tz = []
    qx = []
    qy = []
    qz = []
    qw = []
    with fileinput.input([filename]) as f:
        for i, line in enumerate(f):
            if line.startswith('#'):
                continue
            if i % rate != 0:
                continue
            columns = line.split(' ')
            if len(columns) < 7:
                print('Warning: Invalid line', line)
                continue
            tx.append(float(columns[-7]))
            ty.append(float(columns[-6]))
            tz.append(float(columns[-5]))
            qx.append(float(columns[-4]))
            qy.append(float(columns[-3]))
            qz.append(float(columns[-2]))
            qw.append(float(columns[-1]))
    position = np.transpose(np.array([tx, ty, tz]))
    orientation = quaternion.as_quat_array(np.transpose(np.array([qx, qy, qz, qw])))
    return position, orientation



def position_to_ply(position: np.array, colour) -> str:
    num_vertices = position.shape[0]
    r, g, b = colour[0], colour[1], colour[2]
    ply_header = '\n'.join([
        "ply",
        "format ascii 1.0",
        "comment TUM trajectory",
        "element vertex " + str(num_vertices) + "",
        "property float x",
        "property float y",
        "property float z",
        "property uint red",
        "property uint green",
        "property uint blue",
        "element edge " + str(num_vertices - 1) + "",
        "property int vertex1",
        "property int vertex2",
        "end_header"
    ])
    ply_vertices = '\n'.join(['{} {} {} {} {} {}'.format(x[0], x[1], x[2], r, g, b) for x in position])
    ply_edges = '\n'.join(['{} {}'.format(i, i + 1) for i in range(num_vertices - 1)])
    return '\n'.join([ply_header, ply_vertices, ply_edges])



if __name__ == "__main__":
    try:
        args = parse_args()
        position, _ = read_poses(args.infile, args.pose_rate)
        ply_str = position_to_ply(position, hex_to_rgb(args.colour))
        if args.outfile is None:
            print(ply_str)
        else:
            with open(args.outfile, 'w') as f:
                f.write(ply_str)
    except KeyboardInterrupt:
        pass

