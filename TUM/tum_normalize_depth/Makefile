# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: CC0-1.0

# tum_normalize_depth Makefile

# Executable name
EXECUTABLE = bin/tum_normalize_depth

# Source files to compile
INCLUDEDIR = include
SRCDIR = src
CXXSOURCES = $(wildcard $(SRCDIR)/*.cpp)
CXXOBJECTS = $(CXXSOURCES:.cpp=.o)

# Preprocessor, compiler and linker flags
CPPFLAGS +=
CFLAGS = -I$(INCLUDEDIR) -std=c++17 -Wall -Wextra -pedantic -fopenmp
LDLIBS = -lstdc++ -lstdc++fs
# Use the first available OpenCV version
CFLAGS += $(shell if pkg-config --exists opencv; then pkg-config --cflags opencv; else pkg-config --cflags opencv4; fi)
LDLIBS += $(shell if pkg-config --exists opencv; then pkg-config --libs opencv; else pkg-config --libs opencv4; fi)

PREFIX ?= /usr/local
_INSTDIR = $(DESTDIR)$(PREFIX)
BINDIR ?= $(_INSTDIR)/bin



.PHONY: all
all: release

$(SRCDIR)/lodepng.o: $(SRCDIR)/lodepng.cpp $(INCLUDEDIR)/lodepng.h
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(SRCDIR)/lodepng.cpp

$(SRCDIR)/tum_normalize_depth_utils.o: $(SRCDIR)/tum_normalize_depth_utils.cpp
$(SRCDIR)/tum_normalize_depth_utils.o: $(INCLUDEDIR)/tum_normalize_depth_utils.hpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(SRCDIR)/tum_normalize_depth_utils.cpp

$(SRCDIR)/tum_normalize_depth_options.o: $(SRCDIR)/tum_normalize_depth_options.cpp
$(SRCDIR)/tum_normalize_depth_options.o: $(INCLUDEDIR)/tum_normalize_depth_options.hpp
$(SRCDIR)/tum_normalize_depth_options.o: $(INCLUDEDIR)/tum_normalize_depth_filesystem.hpp
$(SRCDIR)/tum_normalize_depth_options.o: $(INCLUDEDIR)/tum_normalize_depth_utils.hpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(SRCDIR)/tum_normalize_depth_options.cpp

$(SRCDIR)/tum_normalize_depth.o: $(SRCDIR)/tum_normalize_depth.cpp
$(SRCDIR)/tum_normalize_depth.o: $(INCLUDEDIR)/tum_normalize_depth.hpp
$(SRCDIR)/tum_normalize_depth.o: $(INCLUDEDIR)/tum_normalize_depth_options.hpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(SRCDIR)/tum_normalize_depth.cpp

$(SRCDIR)/tum_normalize_depth_main.o: $(SRCDIR)/tum_normalize_depth_main.cpp
$(SRCDIR)/tum_normalize_depth_main.o: $(INCLUDEDIR)/tum_normalize_depth.hpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) -c -o $@ $(SRCDIR)/tum_normalize_depth_main.cpp

$(EXECUTABLE): $(CXXOBJECTS)
	mkdir -p bin
	$(CXX) $(CFLAGS) $(CPPFLAGS) -o $(EXECUTABLE) $(CXXOBJECTS) $(LDLIBS)

.PHONY: release
release: CFLAGS += -DNDEBUG -O3
release: $(EXECUTABLE)

.PHONY: debug
debug: CFLAGS += -g -Werror
debug: $(EXECUTABLE)


.PHONY: install
install: release
	install -D $(EXECUTABLE) $(BINDIR)/$(EXECUTABLE)

.PHONY: uninstall
uninstall:
	rm -f $(BINDIR)/$(EXECUTABLE)

.PHONY: clean
clean:
	rm -rf $(EXECUTABLE)
	rm -rf *.o
	rm -rf $(SRCDIR)/*.o

