# tum\_normalize\_depth

Normalize the depth images of a [TUM RGB-D
dataset](https://vision.in.tum.de/data/datasets/rgbd-dataset) and save them in separate folders.
This is useful when visualizing a run through a TUM dataset since the depth images are hard to see
otherwise.



## Build

To compile the executable and place it inside the `bin/` directory just run
`make`.



## Usage

To normalize a dataset contained in `/path/to/rgbd_dataset_freiburg1_desk/` run

``` bash
./bin/tum_normalize_depth /path/to/rgbd_dataset_freiburg1_desk/
```

All grayscale images in `depth*` and `unc*` folders will be processed and saved in corresponding
folders with the `_n` suffix.

For the program help run

``` bash
./bin/tum_normalize_depth --help
```

You can customize the normalized images by inverting them or applying a specific colormap to them.
To do this edit the default option values in `Options::Options()` located in
`src/tum_normalize_depth_options.cpp` and recompile. The available colormap names are documented in
[OpenCV](https://docs.opencv.org/master/d3/d50/group__imgproc__colormap.html).



## License

Copyright © 2020 Smart Robotics Lab, Imperial College London

Distributed under the BSD 3-Clause license.

