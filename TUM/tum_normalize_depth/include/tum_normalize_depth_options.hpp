/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __TUM_NORMALIZE_DEPTH_OPTIONS_HPP
#define __TUM_NORMALIZE_DEPTH_OPTIONS_HPP

#include <iostream>
#include <map>
#include <stdint.h>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>

#include "tum_normalize_depth_filesystem.hpp"



/** The std::maps allow handling multiple kinds of grayscale images at the same time. Currently
 * depth and uncertainty images are being used with "depth" and "uncert" as keys respectively.
 */
struct Options {
    stdfs::path                                     input_dir;
    std::map<std::string, std::vector<stdfs::path>> directories;
    std::map<std::string, std::vector<stdfs::path>> images;
    std::map<std::string, uint16_t>                 min_value;
    std::map<std::string, uint16_t>                 max_value;
    std::map<std::string, cv::ColormapTypes>        colormap;
    std::map<std::string, bool>                     enable_colormap;
    std::map<std::string, bool>                     invert;

    Options();
};



/** Stream operator for Options struct.
 */
std::ostream& operator<<(std::ostream& out, const Options& opts);



/** Parse the command line arguments and return a struct containing them.
 */
Options parse_args(int argc, char** argv);

#endif // __TUM_NORMALIZE_DEPTH_OPTIONS_HPP

