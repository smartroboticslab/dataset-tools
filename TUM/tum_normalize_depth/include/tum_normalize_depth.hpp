/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __TUM_NORMALIZE_DEPTH_HPP
#define __TUM_NORMALIZE_DEPTH_HPP

#include "tum_normalize_depth_options.hpp"



/** Find all the depth and uncertainty subdirectories of Options::input_dir and store in
 * Options::directories.
 */
void get_directories(Options& options);

/** Find all grayscale PNG images in Options::directories and store in Options::images.
 */
void get_grayscale_images(Options& options);

/** Normalize all images in Options::images and save in separate directories.
 */
void process_images(const Options& options);

#endif // __TUM_NORMALIZE_DEPTH_HPP

