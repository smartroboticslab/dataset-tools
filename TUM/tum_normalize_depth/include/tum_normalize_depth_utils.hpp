/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __TUM_NORMALIZE_UTILS_HPP
#define __TUM_NORMALIZE_UTILS_HPP

#include <string>



bool begins_with(const std::string& s, const std::string& prefix);

bool ends_with(const std::string& s, const std::string& suffix);

/** Prepend "Warning: " to s wrapped in ANSI escape sequences to appear in yellow.
 */
std::string warning_str(const std::string& s);

/** Prepend "Error: " to s wrapped in ANSI escape sequences to appear in red.
 */
std::string error_str(const std::string& s);

#endif // __TUM_NORMALIZE_UTILS_HPP

