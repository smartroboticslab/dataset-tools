/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <argp.h>
#include <stdlib.h>

#include "tum_normalize_depth_options.hpp"
#include "tum_normalize_depth_utils.hpp"



Options::Options() {
    directories["depth"] = std::vector<stdfs::path>();
    directories["uncert"] = std::vector<stdfs::path>();
    images["depth"] = std::vector<stdfs::path>();
    images["uncert"] = std::vector<stdfs::path>();
    min_value["depth"] = UINT16_MAX;
    min_value["uncert"] = UINT16_MAX;
    max_value["depth"] = 0;
    max_value["uncert"] = 0;
    colormap["depth"] = cv::COLORMAP_SUMMER;
    colormap["uncert"] = cv::COLORMAP_PINK;
    enable_colormap["depth"] = true;
    enable_colormap["uncert"] = true;
    invert["depth"] = false;
    invert["uncert"] = false;
}



std::ostream& operator<<(std::ostream& out, const Options& options) {
    // Depth/Uncertainty
    for (const auto& p : options.directories) {
        const std::string& type = p.first;
        const std::vector<stdfs::path>& dirs = p.second;
        out << type << "\n"
            << "  " << type << " min:                  " << options.min_value.at(type) << "\n"
            << "  " << type << " max:                  " << options.max_value.at(type) << "\n"
            << "  " << type << " enable colormap:      " << (options.enable_colormap.at(type) ? "YES" : "NO") << "\n"
            << "  " << type << " invert:               " << (options.invert.at(type) ? "YES" : "NO") << "\n";
        out << "  " << type << " directories:          " << "\n";
        for (const auto& dir : dirs) {
            std::cout << "    " << dir << "\n";
        }
        out << "  " << type << " images:               " << options.images.at(type).size() << "\n";
    }
    return out;
}



static error_t argp_parser(int key, char* arg, struct argp_state* state) {
    Options* options = static_cast<Options*>(state->input);
    switch (key) {
        case ARGP_KEY_ARG:
            if (state->arg_num > 1) {
                std::cerr << error_str("too many arguments\n");
                argp_usage(state);
            }
            options->input_dir = stdfs::path(arg);
            break;

        case ARGP_KEY_END:
            if (state->arg_num < 1) {
                std::cerr << error_str("too few arguments\n");
                argp_usage(state);
            }
            // Ensure a directory was provided
            if (!stdfs::is_directory(options->input_dir)) {
                std::cerr << error_str("") << options->input_dir << " is not a directory\n";
                argp_usage(state);
            }
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}



Options parse_args(int argc, char** argv) {
    // Program help and command line argument declaration
    const char doc[] = "\nNormalize the depth images of a TUM dataset and save them in separate folders.\n\n"
        "DIRECTORY is the top-level TUM dataset directory, containing the "
        "rgb.txt, depth.txt and groundtruth.txt files. All depth* and unc* "
        "folders inside it will be processed and saved in folders with the "
        "_n suffix.";
    const char args_doc[] = "DIRECTORY";
    const struct argp_option argp_options[] = {{0, 0, 0, 0, 0, 0}};

    Options options;
    const struct argp argp_settings = {argp_options, argp_parser, args_doc, doc, 0, 0, 0};
    argp_parse(&argp_settings, argc, argv, 0, 0, &options);
    return options;
}

