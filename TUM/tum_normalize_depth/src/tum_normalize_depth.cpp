/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <algorithm>

#include "tum_normalize_depth.hpp"
#include "tum_normalize_depth_utils.hpp"
#include "tum_normalize_depth_filesystem.hpp"



cv::Mat valid_depth_mask(const cv::Mat& image) {
    cv::Mat nonzero_mask;
    cv::compare(image, cv::Mat::zeros(image.rows, image.cols, image.type()), nonzero_mask, cv::CmpTypes::CMP_NE);
    // NOTE Some completed depth images have values of 65532 and 65535
    cv::Mat nonmax_mask;
    cv::compare(image, cv::Mat(image.rows, image.cols, image.type(), 65000), nonmax_mask, cv::CmpTypes::CMP_LT);
    cv::Mat mask;
    cv::bitwise_and(nonzero_mask, nonmax_mask, mask);
    return mask;
}



cv::Mat invalid_depth_mask(const cv::Mat& image) {
    cv::Mat mask = valid_depth_mask(image);
    cv::bitwise_not(mask, mask);
    return mask;
}



std::pair<uint16_t,uint16_t> image_minmax(const cv::Mat& image) {
    const cv::Mat valid_mask = valid_depth_mask(image);
    double img_min, img_max;
    cv::minMaxIdx(image, &img_min, &img_max, nullptr, nullptr, valid_mask);
    if (img_min < img_max) {
        return std::make_pair<uint16_t,uint16_t>(static_cast<uint16_t>(img_min), static_cast<uint16_t>(img_max));
    } else {
        return std::make_pair<uint16_t,uint16_t>(UINT16_MAX, 0);
    }
}



void normalize_image(cv::Mat& image, uint16_t depth_min, uint16_t depth_max) {

    // Compute a mask for the invalid depth pixels
    const cv::Mat invalid_mask = invalid_depth_mask(image);

    // Normalize the image
    const uint16_t range = depth_max - depth_min;
    const float scale = static_cast<float>(UINT16_MAX) / range;
    image = scale * (image - depth_min);

    // Set the invalid depth pixels to 0
    image.setTo(0, invalid_mask);
}



void invert_image(cv::Mat& image, bool invert) {
    if (invert) {
        // Compute a mask for the invalid depth pixels
        const cv::Mat invalid_mask = invalid_depth_mask(image);
        // Invert the image
        cv::bitwise_not(image, image);
        // Set the invalid depth pixels to 0
        image.setTo(0, invalid_mask);
    }
}



void colorize_image(cv::Mat& image, cv::ColormapTypes colormap, bool enable_colormap) {
    if (enable_colormap) {
        cv::Mat image_8u;
        image.convertTo(image_8u, CV_8U, 1.0 / UINT8_MAX);
        cv::Mat image_c;
        cv::applyColorMap(image_8u, image_c, colormap);
        // Set invalid depth measurements to black. Create a mask by converting the depth image
        // into a binary image and inverting it.
        cv::Mat invalid_depth_mask;
        cv::threshold(image_8u, invalid_depth_mask, 0, UINT8_MAX, cv::THRESH_BINARY_INV);
        image_c.setTo(cv::Scalar(0, 0, 0), invalid_depth_mask);
        image_c.copyTo(image);
    }
}



void get_directories(Options& options) {
    // Iterate over all directory contents
    for (const auto& path : stdfs::directory_iterator(options.input_dir)) {
        if (stdfs::is_directory(path)) {
            // Extract the directory name from the path, e.g. /foo/bar -> bar
            const std::string dirname = path.path().stem().string();
            // Add it to the depth or uncertainty directories
            if (begins_with(dirname, "depth") && !ends_with(dirname, "_norm")) {
                options.directories["depth"].push_back(path.path());
            } else if (begins_with(dirname, "unc") && !ends_with(dirname, "_norm")) {
                options.directories["uncert"].push_back(path.path());
            }
        }
    }

    // Ensure at least one directory was found
    if (std::all_of(options.directories.begin(), options.directories.end(),
                [](const auto& x){ return x.second.empty(); })) {
        std::cerr << error_str("") << options.input_dir
            << " does not contain any depth or uncertainty directories\n";
        exit(EXIT_FAILURE);
    }
}



void get_grayscale_images(Options& options) {
    // Iterate over both directory types (depth/uncertainty)
    for (const auto& p : options.directories) {
        const std::string& type = p.first;
        const std::vector<stdfs::path> dirs = p.second;
        // Iterate over all directories for this type
        for (const auto& dir : dirs) {
            // Iterate over all files in dir
            bool image_found = false;
            for (const auto& path : stdfs::directory_iterator(dir)) {
                // Find PNG files
                if (stdfs::is_regular_file(path) && path.path().extension() == ".png") {
                    const std::string file = path.path().string();
                    const cv::Mat image = cv::imread(file.c_str(), cv::IMREAD_UNCHANGED);
                    // Only keep grayscale PNG images
                    if (image.data != nullptr && image.channels() == 1) {
                        options.images[type].push_back(path.path());
                        image_found = true;
                        // Compute the minimum and maximum depth values since we opened the image
                        const std::pair<uint16_t,uint16_t> v = image_minmax(image);
                        options.min_value[type] = std::min(options.min_value[type], v.first);
                        options.max_value[type] = std::max(options.max_value[type], v.second);
                    }
                }
            }
            if (!image_found) {
                std::cerr << warning_str("no images found in ") << dir << "\n";
                // Remove it from the directories in options. We are using a copy here so that's
                // safe.
                std::vector<stdfs::path>& v = options.directories[type];
                v.erase(std::remove(v.begin(), v.end(), dir), v.end());
            }
        }
    }
}



void process_images(const Options& options) {
    // Create the output directories for all types
    for (const auto& p : options.directories) {
        const std::vector<stdfs::path>& dirs = p.second;
        for (const auto& dir : dirs) {
            stdfs::path outdir = dir;
            outdir += stdfs::path("_norm");
            stdfs::create_directory(outdir);
        }
    }

    // Process all images
    for (const auto& p : options.images) {
        const std::string& type = p.first;
        const std::vector<stdfs::path>& images = p.second;
        // Read each image
#pragma omp parallel for
        for (const auto& path : images) {
            cv::Mat image = cv::imread(path.string(), cv::IMREAD_UNCHANGED);
            // Process the image
            normalize_image(image, options.min_value.at(type), options.max_value.at(type));
            invert_image(image, options.invert.at(type));
            colorize_image(image, options.colormap.at(type), options.enable_colormap.at(type));
            // Compute the filename of the output image
            stdfs::path parent_dir (path.parent_path());
            parent_dir += stdfs::path("_norm");
            const stdfs::path outfile = parent_dir / path.filename();
            // Save it
            if (!cv::imwrite(outfile.string(), image)) {
                std::cout << error_str("could not write image ") << outfile.string() << "\n";
            }
        }
    }
}

