/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "tum_normalize_depth_utils.hpp"



bool begins_with(const std::string& s, const std::string& prefix) {
    if (s.size() >= prefix.size()) {
        return (s.compare(0, prefix.length(), prefix) == 0);
    } else {
        return false;
    }
}



bool ends_with(const std::string& s, const std::string& suffix) {
    if (s.size() >= suffix.size()) {
        return (s.compare(s.length() - suffix.length(), suffix.length(), suffix) == 0);
    } else {
        return false;
    }
}



std::string warning_str(const std::string& s) {
    return "\x1B[1;93mWarning:\x1B[m " + s;
}



std::string error_str(const std::string& s) {
    return "\x1B[1;91mError:\x1B[m " + s;
}

