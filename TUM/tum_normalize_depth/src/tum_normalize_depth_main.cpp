/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "tum_normalize_depth.hpp"



int main(int argc, char** argv) {
    Options options = parse_args(argc, argv);

    std::cout << "Discovering images...\n";
    get_directories(options);
    get_grayscale_images(options);
    std::cout << options;

    std::cout << "Processing images...\n";
    process_images(options);

    exit(EXIT_SUCCESS);
}

