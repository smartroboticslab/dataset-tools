#!/bin/sh
# Test if a directory contains a valid TUM dataset
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# tum_validate.sh
# Test if a directory contains a valid TUM dataset.
# The script will check that:
# - The TUM text files exist (depth.txt, rgb.txt, groundtruth.txt).
# - The TUM text files contain valid data (correct number of columns and valid
#   column data).
# - The depth and RGB images referenced in the text files exist.
# - The depth and RGB images referenced in the text files are in the correct
#   formats (640x480 and 16-bit grayscale/8-bit RGB).
# The script will print messages for any errors found.
#
# Validate directory dir
#     tum_validate.sh dir

set -eu
IFS="$(printf '%b_' '\t\n')"; IFS="${IFS%_}"

readonly float_regex='[+-]?([0-9]+(\.[0-9]*)?|\.[0-9]+)'

readonly positive_float_regex='[+]?([0-9]+(\.[0-9]*)?|\.[0-9]+)'

# Count data (non-comment) lines and print a message if none were found
# Ensure each data line has 2 columns
# Ensure the timestamp is a positive float, skip the following tests otherwise
# Ensure the filename format is depth/TIMESTAMP.png
readonly validate_depth_txt='
/^[^#]/ { data_lines = data_lines + 1 }
/^[^#]/ && NF != 2 { print "  Expected 2 data columns in line", NR }
/^[^#]/ && $1 !~ /^'"$positive_float_regex"'$/ { print "  Invalid timestamp in line", NR; next }
/^[^#]/ && $2 !~ "^depth/" $1 ".png$" { print "  Filename does not match timestamp in line", NR }
END { if (data_lines == 0) print "  No data lines" }'

# Count data (non-comment) lines and print a message if none were found
# Ensure each data line has 2 columns
# Ensure the timestamp is a positive float, skip the following tests otherwise
# Ensure the filename format is rgb/TIMESTAMP.png
readonly validate_rgb_txt='
/^[^#]/ { data_lines = data_lines + 1 }
/^[^#]/ && NF != 2 { print "  Expected 2 data columns in line", NR }
/^[^#]/ && $1 !~ /^'"$positive_float_regex"'$/ { print "  Invalid timestamp in line", NR; next }
/^[^#]/ && $2 !~ "^rgb/" $1 ".png$" { print "  Filename does not match timestamp in line", NR }
END { if (data_lines == 0) print "  No data lines" }'

# Count data (non-comment) lines and print a message if none were found
# Ensure each data line has 8 columns
# Ensure the timestamp is a positive float
# Ensure that tx, ty, tz, qx, qy, qz, qw are floats
readonly validate_groundtruth_txt='
/^[^#]/ { data_lines = data_lines + 1 }
/^[^#]/ && NF != 8 { print "  Expected 8 data columns in line", NR }
/^[^#]/ && $1 !~ /^'"$positive_float_regex"'$/ { print "  Invalid timestamp in line", NR }
/^[^#]/ && $2 !~ /^'"$float_regex"'$/ { print "  Invalid tx in line", NR }
/^[^#]/ && $3 !~ /^'"$float_regex"'$/ { print "  Invalid ty in line", NR }
/^[^#]/ && $4 !~ /^'"$float_regex"'$/ { print "  Invalid tz in line", NR }
/^[^#]/ && $5 !~ /^'"$float_regex"'$/ { print "  Invalid qx in line", NR }
/^[^#]/ && $6 !~ /^'"$float_regex"'$/ { print "  Invalid qy in line", NR }
/^[^#]/ && $7 !~ /^'"$float_regex"'$/ { print "  Invalid qz in line", NR }
/^[^#]/ && $8 !~ /^'"$float_regex"'$/ { print "  Invalid qw in line", NR }
END { if (data_lines == 0) print "  No data lines" }'

usage() {
	echo "Usage: $(basename "$0") DIRECTORY
  Test if DIRECTORY contains a valid TUM dataset"
}

# Given a directory and depth|rgb, ensure all depth/RGB images are valid
validate_images() {
	# Read each line in the supplied file
	while IFS= read -r line || [ -n "$line" ]; do
		# Read the filename from the line, ignore comment lines
		file=$(echo "$line" | grep -v '^#' | awk '{ print $2 }')
		if [ -n "$file" ]; then
			# Prepend the directory
			file="$1/$file"
			# Ensure the file exists
			if [ ! -f "$file" ]; then
				echo "Error: could not find file $file"
				continue
			fi
			# Ensure the file is a valid TUM image
			if ! "valid_$2_image" "$file"; then
				echo "Error: invalid image $file"
			fi
		fi
	done < "$1/$2.txt"
}

# Test if the supplied depth image is a valid TUM PNG
valid_depth_image() {
	set +e
	identify "$1" | grep -q 'PNG 640x480 .* 16-bit Grayscale Gray'
	r="$?"
	set -e
	return "$r"
}

# Test if the supplied RGB image is a valid TUM PNG
valid_rgb_image() {
	set +e
	identify "$1" | grep -q 'PNG 640x480 .* 8-bit sRGB'
	r="$?"
	set -e
	return "$r"
}




# Ensure a single directory was supplied
if [ "$#" -ne 1 ] || [ ! -d "$1" ]; then
	usage
	exit 2
fi

# Remove any trailing slashes from the input directory
readonly dir=${1%%/}

# The txt files to validate
txt_files=$(printf '%s/depth.txt\n%s/rgb.txt\n%s/groundtruth.txt' "$dir" "$dir" "$dir")

# Validate each of the .txt files and keep track of any failed tests
failed=0
for file in $txt_files; do
	# Ensure the file exits
	if [ ! -f "$file" ]; then
		echo "Error: could not find file $file"
		continue
	fi
	# Run awk with the appropriate txt validation script depending on the file name and validate the
	# images
	case "$file" in
		*depth.txt)
			out_txt=$(awk "$validate_depth_txt" "$file")
			out_img=$(validate_images "$dir" "depth")
			;;
		*rgb.txt)
			out_txt=$(awk "$validate_rgb_txt" "$file")
			out_img=$(validate_images "$dir" "rgb")
			;;
		*groundtruth.txt)
			out_txt=$(awk "$validate_groundtruth_txt" "$file")
			out_img=''
			;;
	esac
	# Print any errors
	if [ -n "$out_txt" ]; then
		failed=1
		echo "Errors in $file"
		echo "$out_txt"
	fi
	if [ -n "$out_img" ]; then
		failed=1
		echo "$out_img"
	fi
done

if [ "$failed" -eq 0 ]; then
	echo "Valid TUM dataset in $dir"
fi
exit "$failed"

