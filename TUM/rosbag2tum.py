#!/usr/bin/env python3
import argparse
import cv2
import cv_bridge
import glob
import math
import numpy
import os
import os.path
import rosbag


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("rosbag", metavar="ROSBAG",
        help="the rosbag file to convert")
    parser.add_argument("dir", metavar="DIRECTORY",
        help="the directory where the TUM dataset will be saved")
    parser.add_argument("depth", metavar="DEPTH_TOPIC",
        help="the depth image topic")
    parser.add_argument("rgb", metavar="RGB_TOPIC",
        help="the RGB image topic")
    parser.add_argument("pose", metavar="POSE_TOPIC",
        help="the camera pose topic")
    parser.add_argument("-f", "--from", metavar="TIMESTAMP", type=float,
        default=0.0, dest="start_time",
        help="the timestamp (SECONDS.NANOSECONDS) to start the conversion from")
    parser.add_argument("-t", "--to", metavar="TIMESTAMP", type=float,
        default=math.inf, dest="end_time",
        help="the timestamp (SECONDS.NANOSECONDS) to end the conversion at")
    args = parser.parse_args()
    args.topics = [args.depth, args.rgb, args.pose]
    return args

def msg_to_filename(msg):
    return "{:.6f}.png".format(msg.header.stamp.secs + msg.header.stamp.nsecs/1000000000)

def msg_to_pose_txt(msg):
    s = msg.header.stamp.secs
    u = int(msg.header.stamp.nsecs/1000)
    if hasattr(msg, "pose"):
        tx = msg.pose.position.x
        ty = msg.pose.position.y
        tz = msg.pose.position.z
        qx = msg.pose.orientation.x
        qy = msg.pose.orientation.y
        qz = msg.pose.orientation.z
        qw = msg.pose.orientation.w
    else:
        tx = msg.transform.translation.x
        ty = msg.transform.translation.y
        tz = msg.transform.translation.z
        qx = msg.transform.rotation.x
        qy = msg.transform.rotation.y
        qz = msg.transform.rotation.z
        qw = msg.transform.rotation.w
    return "{}.{:06d} {:.4f} {:.4f} {:.4f} {:.4f} {:.4f} {:.4f} {:.4f}\n".format(
            s, u, tx, ty, tz, qx, qy, qz, qw)

def save_depth(msg, directory: str, bridge):
    filename="/".join([directory, msg_to_filename(msg)])
    img = 5 * bridge.imgmsg_to_cv2(msg, desired_encoding="mono16")
    img[numpy.where(img == 2**16-1)] = 0
    cv2.imwrite(filename, img)

def save_rgb(msg, directory: str, bridge):
    filename="/".join([directory, msg_to_filename(msg)])
    cv2.imwrite(filename, bridge.imgmsg_to_cv2(msg, desired_encoding="bgr8"))

def generate_image_txt(rosbag: str, directory: str, image_type: str):
    files = glob.glob("{}/{}/*.png".format(directory, image_type))
    timestamps = [os.path.splitext(os.path.basename(f))[0] for f in files]
    timestamps.sort()
    with open("{}/{}.txt".format(directory, image_type), "w") as f:
        f.write("# {} images\n".format(image_type))
        f.write("# file: '{}'\n".format(os.path.basename(rosbag)))
        f.write("# timestamp filename\n")
        for t in timestamps:
            f.write("{} {}/{}.png\n".format(t, image_type, t))

if __name__ == "__main__":
    try:
        args = parse_args()
        bridge = cv_bridge.CvBridge()

        depth_dir = args.dir + "/depth"
        rgb_dir = args.dir + "/rgb"
        os.makedirs(depth_dir, exist_ok=True)
        os.makedirs(rgb_dir, exist_ok=True)

        with open(args.dir + "/groundtruth.txt", "w") as pose:
            pose.write("# ground truth trajectory\n")
            pose.write("# file: '{}'\n".format(os.path.basename(args.rosbag)))
            pose.write("# timestamp tx ty tz qx qy qz qw\n")

            with rosbag.Bag(args.rosbag) as bag:
                for topic, msg, _ in bag.read_messages(topics=args.topics):
                    t = msg.header.stamp.to_sec()
                    if args.start_time <= t and t <= args.end_time:
                        if topic == args.depth:
                            save_depth(msg, depth_dir, bridge)
                        elif topic == args.rgb:
                            save_rgb(msg, rgb_dir, bridge)
                        elif topic == args.pose:
                            pose.write(msg_to_pose_txt(msg))
        generate_image_txt(args.rosbag, args.dir, "depth")
        generate_image_txt(args.rosbag, args.dir, "rgb")
    except KeyboardInterrupt:
        pass
