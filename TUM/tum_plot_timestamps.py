#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# tum_plot_timestamps.py
# Plot the timestamps from a TUM ground truth file to identify time intervals
# with missing data.
#
# Plot the timestamps from the supplied ground truth file
#     tum_plot_timestamps.py /path/to/groundtruth.txt
#
# Show detailed program help
#     tum_plot_timestamps.py --help

import argparse
import fileinput

from typing import List

import matplotlib.pyplot as plt
import numpy as np



def parse_arguments():
    parser = argparse.ArgumentParser(
            description=('Plot the timestamps from a TUM ground truth file '
            'to identify time intervals with missing data.'))
    parser.add_argument(
            'input_file',
            nargs='?',
            type=str,
            metavar='FILE',
            help=('a text file containing lines starting with timestamps. '
                'With no FILE or when FILE is -, read standard input'))
    args = parser.parse_args()
    if not args.input_file:
        args.input_file = '-'
    return args



def read_timestamps(filename: str) -> List[float]:
    timestamps = []
    for line in fileinput.input(filename):
        # Ignore emtpy and comment lines
        if not line or line[0] == '#':
            continue
        if line[0].isdecimal():
            tokens = line.split(' ', 1)
            timestamps.append(float(tokens[0]))
    return timestamps



if __name__ == "__main__":
    args = parse_arguments()

    # Read data and remove offset to make the time start from 0.
    timestamps = read_timestamps(args.input_file)
    offset = timestamps[0]
    timestamps = [x - offset for x in timestamps]

    # Plot the timestamps.
    fig, ax = plt.subplots()
    y = np.ones(len(timestamps))
    plt.stem(timestamps, y, use_line_collection=True)
    ax.set_xlabel('Time (s)')
    plt.tight_layout()
    plt.show()

