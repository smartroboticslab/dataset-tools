#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# tum_plot_ground_truth.py
# Plot the camera trajectory from a TUM ground truth file.
#
# Plot every 10 poses from the supplied ground truth file
#     tum_plot_ground_truth.py -n 10 /path/to/groundtruth.txt
#
# Show detailed program help
#     tum_plot_ground_truth.py --help

import argparse
import fileinput
import matplotlib as mpl
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d
import numpy as np
import os
import sys



def printerr(*args, **kwargs) -> None:
    """Print to stderr prefixed with the program name"""
    error_prefix = os.path.basename(sys.argv[0]) + ': error:'
    print(error_prefix, *args, file=sys.stderr, **kwargs)



def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
            description='Plot the camera trajectory from a TUM ground truth file.')
    parser.add_argument('filename', metavar='FILE', nargs='?', default='-',
            help='With no FILE, or when FILE is -, read standard input.')
    parser.add_argument('-P', '--no-plot-position', action='store_false',
            dest='plot_position', help='Don\'t plot the positions.')
    parser.add_argument('-L', '--no-plot-position-line', action='store_false',
            dest='plot_position_line', help='Don\'t plot the position line.')
    parser.add_argument('-O', '--no-plot-orientation', action='store_false',
            dest='plot_orientation', help='Don\'t plot the orientation.')
    parser.add_argument('-r', '--plot-rate', metavar='N', type=int, default=1,
            help='Plot every N ground truth poses.')
    args = parser.parse_args()
    if args.plot_rate <= 0:
        printerr('The plot rate must be a positive integer')
        sys.exit(2)
    return args



def quat_to_rotm(quaternion):
    qi = quaternion[0]
    qj = quaternion[1]
    qk = quaternion[2]
    qr = quaternion[3]
    return np.array([
        [1 - 2*(qj**2 + qk**2), 2*(qi*qj - qk*qr), 2*(qi*qk + qj*qr)],
        [2*(qi*qj + qk*qr), 1 - 2*(qi**2 + qk**2), 2*(qj*qk - qi*qr)],
        [2*(qi*qk - qj*qr), 2*(qj*qk + qi*qr), 1 - 2*(qi**2 + qj**2)],
    ])



def read_poses(filename: str, rate: int):
    tx = []
    ty = []
    tz = []
    qx = []
    qy = []
    qz = []
    qw = []
    with fileinput.input([filename]) as f:
        for i, line in enumerate(f):
            if line.startswith('#'):
                continue
            if i % rate != 0:
                continue
            columns = line.split(' ')
            if len(columns) < 7:
                print('Warning: Invalid line', line)
                continue
            tx.append(float(columns[-7]))
            ty.append(float(columns[-6]))
            tz.append(float(columns[-5]))
            qx.append(float(columns[-4]))
            qy.append(float(columns[-3]))
            qz.append(float(columns[-2]))
            qw.append(float(columns[-1]))
    position = np.transpose(np.array([tx, ty, tz]))
    orientation = np.transpose(np.array([qx, qy, qz, qw]))
    return position, orientation



def plot_frame(axes, T_WF, sizes=(1.0, 1.0, 1.0)):
    origin = T_WF[0:3,3]
    if sizes[0] > 0:
        x_axis = T_WF[0:3,0]
        x_vec = np.stack((origin, origin + sizes[0] * x_axis))
        axes.plot(x_vec[:,0], x_vec[:,1], x_vec[:,2], 'r')
    if sizes[1] > 0:
        y_axis = T_WF[0:3,1]
        y_vec = np.stack((origin, origin + sizes[1] * y_axis))
        axes.plot(y_vec[:,0], y_vec[:,1], y_vec[:,2], 'g')
    if sizes[2] > 0:
        z_axis = T_WF[0:3,2]
        z_vec = np.stack((origin, origin + sizes[2] * z_axis))
        axes.plot(z_vec[:,0], z_vec[:,1], z_vec[:,2], 'b')



def plot_orientation(axes, position, orientation):
    for t_WC, q_WC in zip(position, orientation):
        C_WC = quat_to_rotm(q_WC)
        T_WC = np.eye(4)
        T_WC[0:3,0:3] = C_WC
        T_WC[0:3,3] = t_WC
        plot_frame(axes, T_WC, (0, 0, 0.1))



def set_axes_equal_3D(data, ax) -> None:
    m = data.min(axis=0)
    M = data.max(axis=0)
    mid = (M + m) / 2
    half_range = (M - m).max() / 2
    ax.set_xlim(mid[0] - half_range, mid[0] + half_range)
    ax.set_ylim(mid[1] - half_range, mid[1] + half_range)
    ax.set_zlim(mid[2] - half_range, mid[2] + half_range)



if __name__ == "__main__":
    try:
        args = parse_args()

        position, orientation = read_poses(args.filename, args.plot_rate)

        fig = plt.figure()
        axes = fig.add_subplot(projection='3d')

        plot_frame(axes, np.eye(4), (0.5, 0.5, 0.5))
        if args.plot_orientation:
            plot_orientation(axes, position, orientation)
        if args.plot_position_line:
            axes.plot(position[:,0], position[:,1], position[:,2], 'r')
        if args.plot_position:
            axes.scatter(position[:,0], position[:,1], position[:,2],
                    s=mpl.rcParams['lines.markersize']/20, c='k', marker='.')

        if args.filename == '-':
            title = 'stdin'
        else:
            title = args.filename
        axes.set_title(title)
        axes.set_xlabel('x (m)')
        axes.set_ylabel('y (m)')
        axes.set_zlabel('z (m)')
        set_axes_equal_3D(position, axes)
        plt.show()
    except KeyboardInterrupt:
        pass

