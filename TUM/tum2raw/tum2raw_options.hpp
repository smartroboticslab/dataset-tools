/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __TUM2RAW_OPTIONS_HPP
#define __TUM2RAW_OPTIONS_HPP

#include <argp.h>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <string>



/** Struct containing the program options.
 */
struct Options {
    std::string input_dir;
    std::string raw_file;
    std::string association_file;
    int         verbose            = 0;
    double      max_timestamp_dist = 0.02;
};



/** Stream operator for Options struct. It is intended for printing to
 * std:cout.
 */
static std::ostream& operator<<(std::ostream& out, const Options& opts) {
    out << "Input directory:                " << opts.input_dir << "\n"
        << "Output RAW file:                " << opts.raw_file << "\n"
        << "Output association file:        " << opts.association_file << "\n"
        << "Maximum timestamp distance (s): " << opts.max_timestamp_dist << "\n"
        << "Verbosity level:                " << opts.verbose << "\n";
    return out;
}



/** Return a string containing the command used to run the program.
 */
std::string commandline_used(int argc, char** argv) {
    std::string cmd (argv[0]);
    for (int i = 1; i < argc; ++i) {
        cmd += " " + std::string(argv[i]);
    }
    return cmd;
}



/** Parser for argp.
 */
static error_t argp_parser(int key, char* arg, struct argp_state* state) {
    Options* opts = static_cast<Options*>(state->input);

    size_t last_char_idx;
    switch (key) {
        case 'a':
            opts->association_file = arg;
            break;

        case 'd':
            opts->max_timestamp_dist = atof(arg);
            if (opts->max_timestamp_dist < 0.0) {
                std::cerr << "Error: max-dist must be non-negative\n";
                return ARGP_ERR_UNKNOWN;
            }
            break;

        case 'o':
            opts->raw_file = arg;
            break;

        case 'v':
            (opts->verbose)++;
            break;

        case ARGP_KEY_ARG:
            if (state->arg_num > 1) {
                std::cerr << "Error: Too many arguments\n";
                argp_usage(state);
            }
            opts->input_dir = arg;
            break;

        case ARGP_KEY_END:
            if (state->arg_num < 1) {
                std::cerr << "Error: Too few arguments\n";
                argp_usage(state);
            }
            // Strip trailing /.
            last_char_idx = opts->input_dir.size() - 1;
            if (opts->input_dir[last_char_idx] == '/') {
                opts->input_dir.erase(last_char_idx);
            }
            // Set default values to unset options.
            if (opts->raw_file.empty()) {
                opts->raw_file = opts->input_dir + "/scene.raw";
            }
            if (opts->association_file.empty()) {
                opts->association_file = opts->input_dir + "/association.txt";
            }
            // Print the command line options.
            if (opts->verbose >= 2) {
                std::cout << *opts;
            }
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}



/** Parse the command line arguments and return a struct containing them.
 */
Options parse_args(int argc, char** argv) {
    // Program help and command line argument declaration.
    const char doc[] = "\nConvert a TUM dataset to a SLAMBench v1.0 .raw file.\n\n"
        "DIRECTORY is the top-level TUM dataset directory, containing the "
        "rgb.txt, depth.txt and groundtruth.txt files.";
    const char args_doc[] = "DIRECTORY";
    const struct argp_option options[] = {
        {"association", 'a', "FILE", 0,
                "Save the association file in FILE.\n"
                "(Default: DIRECTORY/association.txt)", 0},
        {"max-dist",    'd', "SECONDS",         0,
                "Set the maximum absolute time difference in fractional "
                "seconds between two timestamps to consider them matched.\n"
                "(Default: 0.02)", 0},
        {"output",      'o', "FILE",         0,
                "Save the .raw file in FILE.\n"
                "(Default: DIRECTORY/scene.raw)", 0},
        {"verbose",     'v', 0,                 0,
                "Produce more verbose output. Extra occurrences of this "
                "option, up to 2 total, increase the amount of information "
                "shown.", 0},
        {0, 0, 0, 0, 0, 0}
    };

    Options opts;
    const struct argp argp_settings = {options, argp_parser, args_doc, doc, 0, 0, 0};
    argp_parse(&argp_settings, argc, argv, 0, 0, &opts);
    return opts;
}

#endif

