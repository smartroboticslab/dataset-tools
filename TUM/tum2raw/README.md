# tum2raw

Convert a [TUM RGB-D
dataset](https://vision.in.tum.de/data/datasets/rgbd-dataset) to a [SLAMBench
v1.0 .raw file](https://github.com/pamela-project/slambench).



## Build

To compile the executable and place it inside the `bin/` directory just run
`make`.



## Usage

To convert a dataset contained in `/path/to/rgbd_dataset_freiburg1_desk/` run

``` bash
./bin/tum2raw /path/to/rgbd_dataset_freiburg1_desk/
```

To use the converted dataset with supereight pass
`/path/to/rgbd_dataset_freiburg1_desk/scene.raw` as the input file (`-i `
option) and `/path/to/rgbd_dataset_freiburg1_desk/association.txt` as the
ground truth file (`-g` option).

For the program help run

``` bash
./bin/tum2raw --help
```



## License

Copyright © 2020 Smart Robotics Lab, Imperial College London

Distributed under the BSD 3-Clause license.

