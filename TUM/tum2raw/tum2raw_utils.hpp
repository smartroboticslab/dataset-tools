/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __TUM2RAW_UTILS_HPP
#define __TUM2RAW_UTILS_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>

#include <stdio.h>
#include <unistd.h>

#include "lodepng.h"



/** Struct containing information about a single dataset frame. Each frame
 * corresponds to a single like in some timestamped file.
 *
 * id        An integer frame ID, starting from 0.
 * timestamp The timestamp of the frame in seconds since the epoch.
 * data      The rest of the line contents. It can contain one or more
 *           filenames and/or a ground truth pose, depending on the file it was
 *           read from.
 */
struct FrameInfo {
    size_t      id;
    double      timestamp;
    std::string data;
};



/** Equality operator for FrameInfo because C++ does not create one.
 */
bool operator==(const FrameInfo& lhs, const FrameInfo& rhs) {
    return (lhs.id == rhs.id) && (lhs.timestamp == rhs.timestamp) && (lhs.data == rhs.data);
}



/** Stream operator for FrameInfo struct. It is intended for printing to a
 * file.
 */
static std::ostream& operator<<(std::ostream& out, const FrameInfo& f) {
    // Save the ostream flags.
    std::ios old_state(nullptr);
    old_state.copyfmt(out);
    // ID
    out << std::setw(6) << std::setfill('0') << f.id << " ";
    out.copyfmt(old_state);
    // Timestamp
    out << std::setprecision(6) << std::setiosflags(std::ios_base::fixed) << f.timestamp << " ";
    out.copyfmt(old_state);
    // Data
    out << f.data;
    return out;
}



/** Remove leading and trailing characters from s. Defaults to removing
 * whitespace.
 */
std::string trim(const std::string&s, const std::string& characters = " \t\r\n") {
    const auto begin = s.find_first_not_of(characters);
    if (begin == std::string::npos) {
        // Found only invalid characters
        return "";
    }
    const auto end = s.find_last_not_of(characters);
    const auto length = end - begin + 1;
    return s.substr(begin, length);
}



/** Add ANSI escape sequences to the string so that it's shown coloured in a
 * terminal.
 */
std::string colour_warn(const std::string& s) {
    return "\x1B[93m" + s + "\x1B[m";
}



/** Add ANSI escape sequences to the string so that it's shown coloured in a
 * terminal.
 */
std::string colour_error(const std::string& s) {
    return "\x1B[91m" + s + "\x1B[m";
}



/** Extract a filename beginning with the prefix from the frame data.
 */
std::string get_filename(const FrameInfo&   frame,
                         const std::string& prefix) {
    // Find the start of the first word beginning with prefix.
    const size_t prefix_idx = frame.data.find(prefix);
    if (prefix_idx == std::string::npos) {
        // The prefix was not found.
        return "";
    }
    // Remove the part of the string before the word.
    const std::string word_with_tail = frame.data.substr(prefix_idx);
    // Remove the part of the string after the word.
    const size_t space_idx = word_with_tail.find_first_of(" ");
    const std::string word = word_with_tail.substr(0, space_idx);
    return word;
}



/** Find the frame in frames_to_search that is closest to query_frame. If no
 * frame is found, return a frame with id SIZE_MAX, timestamp 0.0 and an empty
 * string as data.
 */
FrameInfo find_closest_frame(const FrameInfo&              query_frame,
                             const std::vector<FrameInfo>& frames_to_search) {

    // Find the minimum timestamp distance.
    double min_dist = INFINITY;
    size_t min_idx = 0;
    for (size_t i = 0; i < frames_to_search.size(); ++i) {
        const FrameInfo& f = frames_to_search[i];
        const double timestamp_dist = std::fabs(query_frame.timestamp - f.timestamp);
        if (timestamp_dist < min_dist) {
            min_dist = timestamp_dist;
            min_idx = i;
        }
    }

    if (min_dist < INFINITY) {
        return frames_to_search[min_idx];
    } else {
        return {SIZE_MAX, 0.0, ""};
    }
}



/** Return the absolute value of the timestamp distance between the frames.
 * Return NaN if either frame is invalid.
 */
double timestamp_dist(const FrameInfo& frame_1, const FrameInfo& frame_2) {

    const FrameInfo invalid_frame = {SIZE_MAX, 0.0, ""};
    if ((frame_1 == invalid_frame) || (frame_2 == invalid_frame)) {
        return NAN;
    } else {
        return std::fabs(frame_1.timestamp - frame_2.timestamp);
    }
}



/** Read a TUM text file containing lines of timestamped data. Empty lines and
 * lines beginning with # are ignored.
 */
std::vector<FrameInfo> read_timestamped_tum_file(const std::string& filename) {
    std::vector<FrameInfo> frames;

    std::ifstream f (filename);
    if (not f.good()) {
        return frames;
    }

    // Read the file line-by-line.
    size_t line_num = 1;
    size_t frame_id = 0;
    for (std::string line; std::getline(f, line); line_num++, frame_id++) {
        // Ignore empty lines.
        if (line.empty()) {
            continue;
        }
        // Ignore comments.
        if (line[0] == '#') {
            continue;
        }
        // Strip whitespace from the end of the line.
        line = trim(line);
        // Split the line at the first space.
        const size_t space_idx = line.find_first_of(" ");
        const std::string timestamp_s = line.substr(0, space_idx);
        const std::string data = line.substr(space_idx + 1);
        double timestamp;
        try {
            timestamp = std::stod(timestamp_s);
        } catch (const std::exception& e) {
            std::cerr << colour_error("Error") << ": Invalid timestamp in " << filename
                    << ":" << static_cast<long int>(line_num) << "\n";
            frames.clear();
            return frames;
        }
        // Push back the frame data.
        const FrameInfo frame = {frame_id, timestamp, data};
        frames.push_back(frame);
    }
    f.close();

    return frames;
}



/** Write all the FrameInfo structs to the file, one per line.
 */
int write_frame_info(const std::string&            filename,
                     const std::vector<FrameInfo>& frames) {

    std::ofstream f (filename);
    if (not f.good()) {
        std::cerr << colour_error("Error") << ": Could not write file " << filename << "\n";
        return 1;
    }

    // Write the header.
    f << "# Association of rgb images, depth images and ground truth poses\n"
        << "# ID timestamp rgb_filename depth_filename tx ty tz qx qy qz qw\n";

    // Iterate over the associated frames.
    for (const FrameInfo& frame : frames) {
        f << frame << "\n";
    }

    f.close();
    return 0;
}



/** Read a TUM depth PNG image.
 */
int load_tum_depth(const std::string&    filename,
                   std::vector<uint8_t>& data,
                   uint32_t&             width,
                   uint32_t&             height) {

    data.clear();
    width = 0;
    height = 0;

    // Load the raw PNG data.
    std::vector<uint8_t> png_data;
    lodepng::load_file(png_data, filename);

    // Decode the PNG data. The output format must be gray (16 bits per pixel).
    lodepng::State state;
    state.decoder.color_convert = true;
    state.info_raw.colortype = LCT_GREY;
    state.info_raw.bitdepth = 16;
    const unsigned err = lodepng::decode(data, width, height, state, png_data);
    if (err) {
        std::cerr << "LodePNG error for file " << filename << "\n"
                << "  " << lodepng_error_text(err) << "\n";
        return 1;
    }

    // Swap the byte order from big endian to little endian.
    for (size_t i = 0; i < data.size() - 1; i += 2) {
        std::swap(data[i], data[i + 1]);
    }

    return 0;
}



/** Read a TUM RGB PNG image.
 */
int load_tum_rgb(const std::string&    filename,
                 std::vector<uint8_t>& data,
                 uint32_t&             width,
                 uint32_t&             height) {

    data.clear();
    width = 0;
    height = 0;

    // Load the raw PNG data.
    std::vector<uint8_t> png_data;
    lodepng::load_file(png_data, filename);

    // Decode the PNG data. The output format must be RGB (24 bits per pixel).
    lodepng::State state;
    state.decoder.color_convert = true;
    state.info_raw.colortype = LCT_RGB;
    state.info_raw.bitdepth = 8;
    const unsigned err = lodepng::decode(data, width, height, state, png_data);
    if (err) {
        std::cerr << "LodePNG error for file " << filename << "\n"
                << "  " << lodepng_error_text(err) << "\n";
        return 1;
    }

    return 0;
}



/** Scale the TUM depth data to millimeters. This potentially reduces the depth
 * measurement precision but that's what you get for using the .raw file.
 */
void scale_tum_depth(std::vector<uint8_t>& data) {
    // Iterate over each pixel (2 bytes).
    uint16_t* p = reinterpret_cast<uint16_t*>(data.data());
    for (size_t i = 0; i < data.size() / 2; ++i) {
        p[i] /= 5;
    }
}



/** Write a .raw file from the supplied FrameInfo. The PNG images specified in
 * the FrameInfo data will be read and written into the .raw file in order.
 */
int write_raw_file(const Options&                opts,
                   const std::vector<FrameInfo>& frames) {

    std::ofstream f (opts.raw_file, std::ios::binary);
    if (not f.good()) {
        std::cerr << colour_error("Error") << ": Could not write file " << opts.raw_file << "\n";
        return 1;
    }

    // Iterate over the associated frames.
    for (size_t i = 0; i < frames.size(); ++i) {
        const FrameInfo& frame = frames[i];

        // Show the progress only on verbose output or interactive terminals.
        if ((opts.verbose >= 3) || isatty(fileno(stdout))) {
            std::cout << "Frame " << static_cast<long>(i + 1)
                    << "/" << static_cast<long>(frames.size());
            if (opts.verbose >= 3) {
                std::cout << "\n";
            } else {
                // This is an interactive terminal, overwrite the line.
                std::cout << "\r";
                std::cout.flush();
            }
        }

        // Get the depth image filename.
        const std::string depth_filename_part = get_filename(frame, "depth/");
        if (depth_filename_part.empty()) {
            std::cerr << colour_error("Error") << ": Could not find a depth filename in frame\n  "
                << frame << "\n";
        }
        const std::string depth_filename = opts.input_dir + "/" + depth_filename_part;
        if (opts.verbose >= 3) {
            std::cout << depth_filename << "\n";
        }
        // Load the depth image.
        std::vector<uint8_t> depth_data;
        uint32_t depth_width;
        uint32_t depth_height;
        if (load_tum_depth(depth_filename, depth_data, depth_width, depth_height)) {
            return 1;
        }
        // Change from TUM scale to .raw scale.
        scale_tum_depth(depth_data);
        // Write the depth image dimensions and data to the .raw file.
        f.write(reinterpret_cast<const char*>(&depth_width), sizeof(depth_width));
        f.write(reinterpret_cast<const char*>(&depth_height), sizeof(depth_height));
        f.write(reinterpret_cast<const char*>(depth_data.data()), depth_data.size());

        // Get the RGB image filename.
        const std::string rgb_filename_part = get_filename(frame, "rgb/");
        if (rgb_filename_part.empty()) {
            std::cerr << colour_error("Error") << ": Could not find an RGB filename in frame\n  "
                << frame << "\n";
        }
        const std::string rgb_filename = opts.input_dir + "/" + rgb_filename_part;
        if (opts.verbose >= 3) {
            std::cout << rgb_filename << "\n";
        }
        // Load the RGB image.
        std::vector<uint8_t> rgb_data;
        uint32_t rgb_width;
        uint32_t rgb_height;
        if (load_tum_rgb(rgb_filename, rgb_data, rgb_width, rgb_height)) {
            return 1;
        }
        // Write the RGB image dimensions and data to the .raw file.
        f.write(reinterpret_cast<const char*>(&rgb_width), sizeof(rgb_width));
        f.write(reinterpret_cast<const char*>(&rgb_height), sizeof(rgb_height));
        f.write(reinterpret_cast<const char*>(rgb_data.data()), rgb_data.size());
    }
    std::cout << "\n";

    f.close();
    return 0;
}

#endif

