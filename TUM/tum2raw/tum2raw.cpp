/*
 * SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
 * SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "tum2raw_options.hpp"
#include "tum2raw_utils.hpp"

int main(int argc, char** argv) {
    const Options opts = parse_args(argc, argv);

    // Read the txt file data.
    const std::string rgb_txt         = opts.input_dir + "/rgb.txt";
    const std::string depth_txt       = opts.input_dir + "/depth.txt";
    const std::string groundtruth_txt = opts.input_dir + "/groundtruth.txt";
    const std::vector<FrameInfo> rgb_frames = read_timestamped_tum_file(rgb_txt);
    const std::vector<FrameInfo> depth_frames = read_timestamped_tum_file(depth_txt);
    const std::vector<FrameInfo> ground_truth_frames = read_timestamped_tum_file(groundtruth_txt);
    if (rgb_frames.empty()) {
        std::cerr << colour_error("Error") << ": Could not read file " << rgb_txt << "\n";
        exit(EXIT_FAILURE);
    }
    if (depth_frames.empty()) {
        std::cerr << colour_error("Error") << ": Could not read file " << depth_txt << "\n";
        exit(EXIT_FAILURE);
    }
    if (ground_truth_frames.empty()) {
        std::cerr << colour_warn("Warning") << ": Could not read ground truth file " << groundtruth_txt << "\n";
    }

    // Iterate over each RGB image data.
    std::vector<FrameInfo> associated_frames;
    double max_timestamp_dist = 0.0;
    for (const auto& rgbf : rgb_frames) {
        // Find the closest depth image and ground truth pose.
        const FrameInfo nearest_depth = find_closest_frame(rgbf, depth_frames);
        const double nearest_depth_dist = timestamp_dist(rgbf, nearest_depth);
        const FrameInfo nearest_groundtruth = find_closest_frame(rgbf, ground_truth_frames);
        const double nearest_groundtruth_dist = timestamp_dist(rgbf, nearest_groundtruth);
        // Continue to the next frame if no matches were found.
        if (nearest_depth_dist > opts.max_timestamp_dist) {
            if (nearest_depth_dist > max_timestamp_dist) {
                max_timestamp_dist = nearest_depth_dist;
            }
            if (opts.verbose >= 1) {
                std::cerr << colour_warn("Warning")
                        << ": Could not match RGB frame " << static_cast<unsigned long>(rgbf.id)
                        << " to a depth frame. Min timestamp distance "
                        << nearest_depth_dist << " s\n";
            }
            continue;
        }
        if (!ground_truth_frames.empty() && nearest_groundtruth_dist > opts.max_timestamp_dist) {
            if (nearest_groundtruth_dist > max_timestamp_dist) {
                max_timestamp_dist = nearest_groundtruth_dist;
            }
            if (opts.verbose >= 1) {
                std::cerr << colour_warn("Warning")
                        << ": Could not match RGB frame " << static_cast<unsigned long>(rgbf.id)
                        << " to a ground truth pose. Min timestamp distance "
                        << nearest_groundtruth_dist << " s\n";
            }
            continue;
        }
        // Push back the combined frame data.
        FrameInfo associated_frame;
        if (ground_truth_frames.empty()) {
            associated_frame = {associated_frames.size(), rgbf.timestamp,
                    rgbf.data + " " + nearest_depth.data};
        } else {
            associated_frame = {associated_frames.size(), rgbf.timestamp,
                    rgbf.data + " " + nearest_depth.data + " " + nearest_groundtruth.data};
        }
        associated_frames.push_back(associated_frame);
    }

    // Show the matching results and suggest command to match all frames.
    std::cout << "Matched " << associated_frames.size()
            << "/" << rgb_frames.size() << " RGB frames\n";
    if (associated_frames.size() < rgb_frames.size()) {
        std::cout << "The maximum timestamp distance between frames was "
                << max_timestamp_dist << " s\n";
        if (opts.verbose == 0) {
            std::cout << "To see the timestamp distances for all the "
                    << "unmatched frames run\n    "
                    << commandline_used(argc, argv) << " -v\n";
        }
        std::cout << "To match all frames run\n    "
                << commandline_used(argc, argv) << " -d "
                << std::ceil(max_timestamp_dist) << "\n";
    }

    // Write the association file.
    if (write_frame_info(opts.association_file, associated_frames)) {
        exit(EXIT_FAILURE);
    }

    // Write the raw file
    if (write_raw_file(opts, associated_frames)) {
        exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}

