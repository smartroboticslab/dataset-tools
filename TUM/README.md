# TUM scripts

## tum2raw

Convert a [TUM RGB-D
dataset](https://vision.in.tum.de/data/datasets/rgbd-dataset) to a [SLAMBench
v1.0 .raw file](https://github.com/pamela-project/slambench).

For more detailed help see the [dedicated README](./tum2raw/README.md).



## tum\_normalize\_depth

Normalize the depth images of a TUM dataset and save them in separate folders.
This is useful when visualizing a run through a TUM dataset since the depth
images are hard to see otherwise.

For more detailed help see the [dedicated
README](./tum_normalize_depth/README.md).



## tum\_ground\_truth\_to\_ply.py

Generate a PLY file containing the positions from a TUM ground truth file.

``` sh
# Generate a file named path.ply containing every 10 positions from
# /path/to/groundtruth.txt and shown in green in the PLY file
tum_ground_truth_to_ply.py -o path.ply -r 10 -c 00FF00 /path/to/groundtruth.txt

# Show detailed program help
tum_ground_truth_to_ply.py --help
```



## tum\_plot\_ground\_truth.py

Plot the camera trajectory from a ground truth file in the TUM format.

``` sh
# Plot every 10 poses from the supplied ground truth file
tum_plot_ground_truth.py -n 10 /path/to/groundtruth.txt

# Show detailed program help
tum_plot_ground_truth.py --help
```



## tum\_plot\_timestamps.py

Plot the timestamps from a TUM ground truth file to identify time intervals with
missing data.

``` sh
# Plot the timestamps from the supplied ground truth file
tum_plot_timestamps.py /path/to/groundtruth.txt

# Show detailed program help
tum_plot_timestamps.py --help
```




## tum\_unique\_depth\_association.sh

Read a TUM the association file and generate an association file that contains
each depth image only once.

``` sh
# Find the unique depth images from association.txt and save to
# association_unique.txt
tum_unique_depth_association.sh association.txt > association_unique.txt

# Show detailed program help
tum_unique_depth_association.sh -h
```



## tum\_validate.sh

Test if a directory contains a valid TUM dataset.
The script will check that:

- The TUM text files exist (depth.txt, rgb.txt, groundtruth.txt).
- The TUM text files contain valid data (correct number of columns and valid
  column data).
- The depth and RGB images referenced in the text files exist.
- The depth and RGB images referenced in the text files are in the correct
  formats (640x480 and 16-bit grayscale/8-bit RGB).

The script will print messages for any errors found.

``` sh
# Validate directory dir
tum_validate.sh dir
```

