#!/bin/sh
# SPDX-FileCopyrightText: 2019-2021 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2019-2021 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

set -eu
IFS="$(printf '%b_' '\t\n')"; IFS="${IFS%_}"

# Show the program usage on standard output.
usage() {
	printf "Usage: %s [OPTION]... DIRECTORY\n" "$(basename "$0")"
	printf "Download TUM datasets in DIRECTORY.\n"
	# 1. Search the current file for case labels with comments.
	# 2. Remove case syntax.
	# 3. Add leading dash.
	grep -E '[[:space:]].) #' "$0" | sed -e 's/) #/ /g' -e 's/^[ \t]*/    -/g'
}



sequence_urls='https://vision.in.tum.de/rgbd/dataset/freiburg1/rgbd_dataset_freiburg1_xyz.tgz
https://vision.in.tum.de/rgbd/dataset/freiburg1/rgbd_dataset_freiburg1_desk.tgz
https://vision.in.tum.de/rgbd/dataset/freiburg2/rgbd_dataset_freiburg2_desk.tgz
https://vision.in.tum.de/rgbd/dataset/freiburg3/rgbd_dataset_freiburg3_long_office_household.tgz'



# Parse the options.
while getopts 'h' opt_name ; do
	case "$opt_name" in
		h) # Display this help message and exit.
			usage
			exit 0
			;;
		*)
			usage
			exit 2
			;;
	esac
done
# Make $1 the first non-option argument.
shift "$((OPTIND - 1))"

# Parse the arguments.
if [ "$#" -eq 1 ]; then
	# Remove trailing slashes.
	output_dir="${1%%/}"
else
	usage
	exit 2
fi

# Create the output directory.
mkdir -p "$output_dir"
# The file where wget output is logged.
log_file="$output_dir/$(basename "$0").log"
# Clean up the log from any previous invocation.
rm -f "$log_file"

# Download each sequence.
for url in $sequence_urls; do
	filename="$output_dir/$(basename "$url")"
	sequence_name="$(basename "$url")"
	sequence_name="${sequence_name%%.*}"
	sequence_dir="$output_dir/$sequence_name"

	# Skip the sequence if its directory already exists.
	if [ -d "$sequence_dir" ] ; then
		printf "Skipping sequence %s, directory %s already exists\n" \
			"$sequence_name" "$sequence_dir"
		continue
	fi

	# Download the sequence.
	printf "Downloading %s to %s\n" "$url" "$filename"
	wget --no-verbose --append-output "$log_file" --continue \
		--output-document "$filename" "$url"

	# Extract the sequence into its own directory.
	printf "Extracting %s into %s\n" "$filename" "$sequence_dir"
	tar -xzf "$filename" -C "$output_dir"

	# Remove the downloaded file.
	rm "$filename"
done

