#!/bin/sh
# Create an association file containing each depth image only once
# SPDX-FileCopyrightText: 2020 Smart Robotics Lab, Imperial College London
# SPDX-FileCopyrightText: 2020 Sotiris Papatheodorou
# SPDX-License-Identifier: BSD-3-Clause

# tum_unique_depth_association.sh
# Read a TUM the association file and generate an association file that contains
# each depth image only once.
#
# Find the unique depth images from association.txt and save to
# association_unique.txt
#     tum_unique_depth_association.sh association.txt > association_unique.txt
#
# Show detailed program help
#     tum_unique_depth_association.sh -h

set -eu
IFS="$(printf '%b_' '\t\n')"; IFS="${IFS%_}"

usage() {
	echo "Usage: $(basename "$0") INFILE [OUTFILE]
  Read the association file INFILE and output an association file that
  contains each depth image only once to stdout or OUTFILE if supplied
"
	# Search this file for case labels with comments, remove case syntax and add a leading dash
	grep -E '[[:space:]].) #' "$0" | sed 's/) #/ /g' | sed 's/^[ \t]*/    -/g'
}

# Parse command line options
while getopts 'h' opt_name; do
	case "${opt_name}" in
		h) # Display this help message
			usage
			exit 0
			;;
		*)
			usage
			exit 2
			;;
	esac
done

# Make $1 the first non-option argument.
shift "$((OPTIND - 1))"

# Parse the input arguments
case "$#" in
	1)
		infile="$1"
		outfile="/dev/stdout"
		;;
	2)
		infile="$1"
		outfile="$2"
		;;
	*)
		usage
		exit 2
		;;
esac

# Show the comments first
grep '^#' "$infile" > "$outfile"

# Process the non-comments with the following awk program
grep -v '^#' "$infile" | awk '
# Split lines at spaces
BEGIN { FS = " " }
# Find lines where the 4th column (depth) does not match the same column of the previous line.
# prev_depth is inialized to the empty string on first use
($4 != prev_depth) {
	# Print the whole line
	print
	# Store the depth column for comparison with the next line
	prev_depth = $4
}
' >> "$outfile"

